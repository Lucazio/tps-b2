# TP3 : Progressons vers le réseau d'infrastructure
- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
  - [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
      - [Création du routeur](#création-du-routeur)
  - [II. Service d'infra](#ii-service-dinfra)
    - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [B. Setup](#b-setup)
  - [3. Get deeper](#3-get-deeper)
    - [A. DNS forwarder](#a-dns-forwarder)
    - [B. On revient sur la conf du DHCP](#b-on-revient-sur-la-conf-du-dhcp)
  - [III. Services métier](#iii-services-métier)
    - [1. Serveur Web](#1-serveur-web)
    - [2. Partage de fichiers](#2-partage-de-fichiers)
  - [IV. Un peu de théorie: TCP et UDP](#iv-un-peu-de-théorie-tcp-et-udp)
  - [V. Fin](#v-fin)
    - [1.Tableau des réseaux](#1tableau-des-réseaux)
    - [2.Tableau d'adressage](#2tableau-dadressage)
## I. (mini)Architecture réseau
#### Création du routeur
**il a bien un adresse IP dans les 3 réseaux:**
```
[lucas@router ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cb:5b:59 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
    [...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:83:e6:38 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.190/26 brd 10.3.1.191 scope global noprefixroute enp0s8
    [...]
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c4:cb:3f brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.126/25 brd 10.3.1.127 scope global noprefixroute enp0s9
    [...]
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d6:65:f4 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.206/28 brd 10.3.1.207 scope global noprefixroute enp0s10
    [...]
```
**Il a un accès internet:**
```
[lucas@router ~]$ ping -c 4 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=24.10 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=21.3 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 20.058/21.640/24.977/1.993 ms
```
**Il a de la résolution de noms:**
```
[lucas@router ~]$ sudo cat /etc/resolv.conf
nameserver 8.8.8.8
[lucas@router ~]$ ping -c 4 google.com
PING google.com (216.58.198.206) 56(84) bytes of data.
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=1 ttl=114 time=72.9 ms
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=2 ttl=114 time=22.7 ms
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=3 ttl=114 time=140 ms
64 bytes from par10s27-in-f206.1e100.net (216.58.198.206): icmp_seq=4 ttl=114 time=20.3 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 20.282/63.982/140.079/48.705 ms
```
**il porte le nom `router.tp3`:**
```
[lucas@router ~]$ hostname
router.tp3
[lucas@router ~]$ sudo cat /etc/hostname
router.tp3
```
Le routage est activé
```
[lucas@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s10 enp0s9
[lucas@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

## II. Service d'infra
### 1. Serveur DHCP

**Il porte le nom `dhcp.client1.tp3`**
```
[lucas@dhcp ~]$ hostname
dhcp.client1.tp3
[lucas@dhcp ~]$ cat /etc/hostname
dhcp.client1.tp3
```
**Il donne une ip aux machines qui le demandent, leur passerelle et un serveur dns utilisable**
```
[lucas@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;
subnet 10.3.1.128 netmask 255.255.255.192 {
        range 10.3.1.131 10.3.1.189;
        option broadcast-address 10.3.1.191;
        option routers 10.3.1.190;
        option domain-name-servers 8.8.8.8;
}
```
[dhcp.conf](files/dhcpd.conf)
**Mettre en place un client dans le réseau client1**

**Il porte le nom `marcel.client1.tp3`**
```
[lucas@marcel ~]$ hostname
marcel.client1.tp3
[lucas@marcel ~]$ cat /etc/hostname
marcel.client1.tp3
```
**la machine reçoit une IP grâce au serveur DHCP**
```
[lucas@marcel ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:c5:84:e1 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.131/25 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
       valid_lft 429sec preferred_lft 429sec
    inet6 fe80::a00:27ff:fec5:84e1/64 scope link
       valid_lft forever preferred_lft forever
[lucas@marcel ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
BOOTPROTO=dhcp
NAME=enp0s3
DEVICE=enp0s3
ONBOOT=yes
```
**`marcel.client1.tp3` possède une passerelle et une addresse de serveur DNS**
```
[lucas@marcel ~]$ ip r s
default via 10.3.1.190 dev enp0s3 proto dhcp metric 100
10.3.1.128/25 dev enp0s3 proto kernel scope link src 10.3.1.131 metric 100
[lucas@marcel ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 8.8.8.8
```
**`marcel.client1.tp3` a un accès internet + résolution de noms**
```
[lucas@marcel ~]$ ping -c 4 google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=1 ttl=113 time=316 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=2 ttl=113 time=28.1 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=3 ttl=113 time=51.7 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=4 ttl=113 time=21.7 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 21.728/104.506/316.486/122.895 ms
```
**Marcel passe par `router.tp3` pour sortir du réseau**
```
[lucas@marcel ~]$ traceroute 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.190)  1.465 ms  1.402 ms  1.294 ms
 2  10.0.2.2 (10.0.2.2)  1.250 ms  1.137 ms  0.993 ms
 3  10.0.2.2 (10.0.2.2)  1.970 ms  1.882 ms  1.741 ms
```
`10.3.1.190`correspond à `router.tp3`
## 2. Serveur DNS
### B. Setup
**la machine se trouve dans le réseau `server1`**
```
[lucas@dns1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:62:da:85 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.2/25 brd 10.3.1.127 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe62:da85/64 scope link
       valid_lft forever preferred_lft forever
```
son ip est `10.3.1.2/25` dns1 se trouve donc dans le réseau server1

**son nom est `dns1.server1.tp3`**
```
[lucas@dns1 ~]$ hostname
dns1.server1.tp3
[lucas@dns1 ~]$ cat /etc/hostname
dns1.server1.tp3
```
**Il connait l'adresse d'un serveur DNS**
```
[lucas@dns1 ~]$ ping -c 4 google.com
PING google.com (216.58.204.110) 56(84) bytes of data.
64 bytes from par10s28-in-f14.1e100.net (216.58.204.110): icmp_seq=1 ttl=115 time=17.1 ms
64 bytes from par10s28-in-f14.1e100.net (216.58.204.110): icmp_seq=2 ttl=115 time=17.5 ms
64 bytes from par10s28-in-f14.1e100.net (216.58.204.110): icmp_seq=3 ttl=115 time=17.5 ms
64 bytes from par10s28-in-f14.1e100.net (216.58.204.110): icmp_seq=4 ttl=115 time=17.2 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 17.109/17.303/17.480/0.194 ms
[lucas@dns1 ~]$ cat /etc/resolv.conf
nameserver 8.8.8.8
```
**Install du serveur DNS**
```
[lucas@dns1 ~]$ sudo dnf install -y bind bind-utils
[...]
```
**conf du serveur DNS**

**conf de `named.conf`**
```
[lucas@dns1 ~]$ sudo cat /etc/named.conf
options {
        listen-on port 53 { any; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { any; };

        recursion no;

        dnssec-enable yes;
        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

zone "server1.tp3" IN {
        type master;
        file "server1.tp3.forward";
        allow-update { none; };
};

zone "server2.tp3" IN {
        type master;
        file "server2.tp3.forward";
        allow-update { none; };
};
```
**conf des fichiers de zone forward**
```
[lucas@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021080804  ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400       ;Minimum TTL
)

@         IN  NS      dns1.server1.tp3.
@         IN  A       10.3.1.2

dns1           IN  A       10.3.1.2
[lucas@dns1 ~]$ sudo cat /var/named/server2.tp3.forward
$TTL 86400
@   IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021080804  ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400       ;Minimum TTL
)
@       IN NS   dns1.server1.tp3.
@       IN A    10.3.1.2
```
**on lance ensuite le service**
```
[lucas@dns1 ~]$ sudo systemctl enable named
Created symlink /etc/systemd/system/multi-user.target.wants/named.service → /usr/lib/systemd/system/named.service.
[lucas@dns1 ~]$ sudo systemctl start named
```
**on ajoute le service sur le firewall**
```
[lucas@dns1 ~]$ sudo firewall-cmd --add-service=dns --permanent
success
[lucas@dns1 ~]$ sudo firewall-cmd --reload
success
```
**Tester le DNS depuis marcel.client1.tp3**

**définir l'utilisation du serveur DNS**
```
[lucas@marcel ~]$ sudo vim /etc/resolv.conf
[lucas@marcel ~]$ cat /etc/resolv.conf
nameserver 10.3.1.2
```
**Essayer une résolution de nom avec dig**
```
[lucas@marcel ~]$ dig dns1.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 60260
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: ead872ec4b70b89990b0100a6162ce3d3a2dc38ebc3214c8 (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.1.2

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 1 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Sun Oct 10 13:27:56 CEST 2021
;; MSG SIZE  rcvd: 103
```
**prouver que c'est bien votre serveur dns qui répond**
```
[lucas@marcel ~]$ dig dns1.server1.tp3
[...]
;; SERVER: 10.3.1.2#53(10.3.1.2)
[...]
[lucas@dns1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:62:da:85 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.2/25 brd 10.3.1.127 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe62:da85/64 scope link
       valid_lft forever preferred_lft forever
```
**le serveur dns qui répond est bien `dns1.server1.tp3`(10.3.1.2)**

**Appliquer l'utilisation du serveur DNS aux autres noeuds**
```
[lucas@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
[...]
subnet 10.3.1.128 netmask 255.255.255.192 {
[...]
        option domain-name-servers 10.3.1.2;
}
```
## 3. Get deeper
### A. DNS forwarder
**faites en sorte que votre DNS soit désormais un forwarder DNS**
```
[lucas@dns1 ~]$ sudo vim /etc/named.conf
[lucas@dns1 ~]$ sudo cat /etc/named.conf
[...]
        recursion yes;
[...]
```
**`dns1.server1.tp3` est capable de résoudre des noms**
```
[lucas@dns1 ~]$ cat /etc/resolv.conf
nameserver 8.8.8.8
[lucas@dns1 ~]$ ping -c 1 google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from par10s39-in-f14.1e100.net (216.58.214.78): icmp_seq=1 ttl=112 time=204 ms

--- google.com ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 204.282/204.282/204.282/0.000 ms
```
**vérifier depuis `marcel.client1.tp3`**
```
[lucas@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 40489
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 62ee5a6229c7e152a80d30456162d4e5811d9fbc73d29aed (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       142.250.74.238

;; AUTHORITY SECTION:
google.com.             172282  IN      NS      ns2.google.com.
google.com.             172282  IN      NS      ns3.google.com.
google.com.             172282  IN      NS      ns4.google.com.
google.com.             172282  IN      NS      ns1.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         172282  IN      A       216.239.34.10
ns1.google.com.         172282  IN      A       216.239.32.10
ns3.google.com.         172282  IN      A       216.239.36.10
ns4.google.com.         172282  IN      A       216.239.38.10
ns2.google.com.         172282  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172282  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172282  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         172282  IN      AAAA    2001:4860:4802:38::a

;; Query time: 1247 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Sun Oct 10 13:56:20 CEST 2021
;; MSG SIZE  rcvd: 331
```
**le serveur dns ayant répondu est bien `dns1.server1.tp3`(10.3.1.2)**
### B. On revient sur la conf du DHCP
**affiner la configuration du DHCP**

**Faire en sorte que `dhcp.client1.tp3`donne l'adresse du serveur DNS aux clients**
```
[lucas@dhcp ~]$ sudo vim /etc/dhcp/dhcpd.conf
[lucas@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;
subnet 10.3.1.128 netmask 255.255.255.128 {
        range 10.3.1.131 10.3.1.189;
        option broadcast-address 10.3.1.191;
        option routers 10.3.1.190;
        option domain-name-servers 10.3.1.2;
}
[lucas@dhcp ~]$ sudo systemctl restart dhcpd
```
**créer un nouveau client `johnny.client1.tp3`qui récupère tout ça en dhcp**
```
[lucas@johnny ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
[sudo] password for lucas:
BOOTPROTO=dhcp
NAME=enp0s3
DEVICE=enp0s3
ONBOOT=yes
[lucas@johnny ~]$ sudo dhclient -r
[lucas@johnny ~]$ sudo dhclient
[lucas@johnny ~]$ cat /etc/resolv.conf
; generated by /usr/sbin/dhclient-script
nameserver 10.3.1.2
[lucas@johnny ~]$ ping -c 1 dns1.server1.tp3
PING dns1.server1.tp3 (10.3.1.2) 56(84) bytes of data.
64 bytes from 10.3.1.2 (10.3.1.2): icmp_seq=1 ttl=63 time=0.520 ms

--- dns1.server1.tp3 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 0.520/0.520/0.520/0.000 ms
```
## III. Services métier
### 1. Serveur Web
**Setup de `web1.server2.tp3`**

**sur le réseau `server2`**
```
[lucas@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s3
[...]
IPADDR=10.3.1.194
NETMASK=255.255.255.240
```
**son nom est `web1.server2.tp3`**
```
[lucas@web1 ~]$ hostname
web1.server2.tp3
```
**le serveur web que j'ai installé est nginx**
```
[lucas@web1 ~]$ sudo dnf install nginx
[lucas@web1 ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[lucas@web1 ~]$ sudo systemctl start nginx
[lucas@web1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[lucas@web1 ~]$ sudo firewall-cmd --reload
success
```
**Test**
```
[lucas@marcel ~]$ curl web1.server2.tp3
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
[...]
</html>
```
**Le serveur est bien joignable**
### 2. Partage de fichiers
**Setup**

**son nom est `nfs1.server2.tp3`**
```
[lucas@nfs1 ~]$ hostname
nfs1.server2.tp3
```
**il est sur le réseau serveur2**
```
[lucas@nfs1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 
[...]
    inet 10.3.1.195/28 brd 10.3.1.207 scope global noprefixroute enp0s3
[...]
```
**Install NFS**
```
[lucas@nfs1 ~]$ sudo dnf install nfs-utils -y
[...]
[lucas@nfs1 ~]$ sudo vim /etc/idmapd.conf
[lucas@nfs1 ~]$ cat /etc/idmapd.conf
[...]
Domain = server2.tp3
[...]
[lucas@nfs1 ~]$ cd /srv/
[lucas@nfs1 srv]$ sudo mkdir nfs_share
[lucas@nfs1 srv]$ sudo vim /etc/exports
[lucas@nfs1 srv]$ cat /etc/exports
/srv/nfs_share 10.3.1.192/28(rw,no_root_squash)
[lucas@nfs1 srv]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[lucas@nfs1 srv]$ sudo firewall-cmd --add-service=nfs --permanent
success
[lucas@nfs1 srv]$ sudo firewall-cmd --reload
success
[lucas@nfs1 srv]$ sudo chown lucas nfs_share
```
**Setup sur `web1.server2.tp3`**
```
[lucas@web1 ~]$ sudo dnf install nfs-utils -y
[...]
[lucas@web1 ~]$ sudo vim /etc/idmapd.conf
[lucas@nfs1 ~]$ cat /etc/idmapd.conf
[...]
Domain = server2.tp3
[...]
[lucas@web1 ~]$ sudo mkdir /srv/nfs
[lucas@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs
[lucas@web1 ~]$ df -hT
Filesystem                      Type      Size  Used Avail Use% Mounted on
devtmpfs                        devtmpfs  214M     0  214M   0% /dev
tmpfs                           tmpfs     233M     0  233M   0% /dev/shm
tmpfs                           tmpfs     233M  3.4M  230M   2% /run
tmpfs                           tmpfs     233M     0  233M   0% /sys/fs/cgroup
/dev/mapper/rl-root             xfs       6.2G  2.1G  4.2G  34% /
/dev/sda1                       xfs      1014M  182M  833M  18% /boot
tmpfs                           tmpfs      47M     0   47M   0% /run/user/1000
nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.0G  4.2G  33% /srv/nfs
[lucas@web1 ~]$ sudo vim /etc/fstab
[lucas@web1 ~]$ cat /etc/fstab
[...]
nfs1.server2.tp3:/srv/nfs_share /srv/nfs        nfs     defaults        0 0
```
**Test**

**sur `web1.server2.tp3`**
```
[lucas@web1 ~]$ cd /srv/nfs/
[lucas@web1 nfs]$ touch test
[lucas@web1 nfs]$ vim test
[lucas@web1 nfs]$ cat test
Bonjour
```
**sur `nfs1.server2.tp3`**
```
[lucas@nfs1 ~]$ cd /srv/nfs_share/
[lucas@nfs1 nfs_share]$ ls
test
```
## IV. Un peu de théorie: TCP et UDP
**déterminer pour chacun des protocoles si ils sont en TCP ou UDP:**

**SSH**
```
[lucas@web1 ~]$ sudo tcpdump -w tp3_ssh.pcap
PS C:\Users\hurlu> ssh lucas@10.3.1.194
```
Pour le ssh je ne me suis pas connecté en ssh pour ne pas pourrir le réseau et seulement avoir les trames importantes

[tp3_ssh.pcap](files/tp3_ssh.pcap)

**SSH est encapsulé en TCP**

**HTTP**
```
[lucas@web1 ~]$ sudo tcpdump -w tp3_http.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), capture size 262144 bytes
^C12 packets captured
12 packets received by filter
0 packets dropped by kernel
[lucas@marcel ~]$ curl web1.server2.tp3
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
[...]
</html>
```
[tp3_http.pcap](files/tp3_http.pcap)

**HTTP est encapsulé en TCP**

**DNS**
```
[lucas@dns1 ~]$ sudo tcpdump -w tp3_dns.pcap not port 22
[sudo] password for lucas:
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), capture size 262144 bytes
^C8 packets captured
8 packets received by filter
[lucas@marcel ~]$ dig web1.server2.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> web1.server2.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 3934
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: f1b10ae0ba2c9cdda05688d1616c4139888845ab34d47515 (good)
;; QUESTION SECTION:
;web1.server2.tp3.              IN      A

;; ANSWER SECTION:
web1.server2.tp3.       86400   IN      A       10.3.1.194

;; AUTHORITY SECTION:
server2.tp3.            86400   IN      NS      dns1.server1.tp3.

;; ADDITIONAL SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.1.2

;; Query time: 0 msec
;; SERVER: 10.3.1.2#53(10.3.1.2)
;; WHEN: Sun Oct 17 17:28:57 CEST 2021
;; MSG SIZE  rcvd: 132
```
[tp3_dns.pcap](files/tp3_dns.pcap)

**DNS est encapsulé en UDP**

**NFS**
```
[lucas@nfs1 ~]$ sudo tcpdump -w tp3_nfs.pcap not port 22
[sudo] password for lucas:
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), capture size 262144 bytes
^C8 packets captured
8 packets received by filter
0 packets dropped by kernel
[lucas@web1 srv]$ touch nfs/test2
```
[tp3_nfs.pcap](files/tp3_nfs.pcap)

**NFS est encapsulé en TCP**


**Mettre en évidence un 3-way handshake**
pour ça je vais me servir d'un échange http:
```
[lucas@web1 ~]$ sudo tcpdump -w tp3_3way.pcap -c 3 not port 22
[sudo] password for lucas:
dropped privs to tcpdump
tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), capture size 262144 bytes
3 packets captured
11 packets received by filter
0 packets dropped by kernel
[lucas@nfs1 ~]$ curl web1.server2.tp3
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
[...]
</html>
```
| ordre | type trame      | IP source           | MAC source                 | IP destination      | MAC destination            |
| ----- | --------------- | ------------------- | -------------------------- | ------------------- | -------------------------- |
| 1     | TCP  [SYN]      | `nfs1` `10.3.1.195` | `nfs1` `08:00:27:61:9E:EC` | `web1` `10.3.1.194` | `web1` `08:00:27:D7:DB:B7` |
| 2     | TCP  [SYN, ACK] | `web1` `10.3.1.194` | `web1` `08:00:27:D7:DB:B7` | `nfs1` `10.3.1.195` | `nfs1` `08:00:27:61:9E:EC` |
| 3     | TCP  [ACK]      | `nfs1` `10.3.1.195` | `nfs1` `08:00:27:61:9E:EC` | `web1` `10.3.1.194` | `web1` `08:00:27:D7:DB:B7` |
[tp3_3way.pcap](files/tp3_3way.pcap)

## V. Fin
**Schéma**
![schema](img/schema.drawio.png)




### 1.Tableau des réseaux
| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
| ------------- | ----------------- | ----------------- | --------------------------- | ------------------ | ----------------- |
| `client1`     | `10.3.1.128/26`   | `255.255.255.192` | 61                          | `10.3.1.190`       | `10.3.1.191`      |
| `server1`     | `10.3.1.0/25`     | `255.255.255.128` | 125                         | `10.3.1.126`       | `10.3.1.127`      |
| `server2`     | `10.3.1.192/28`   | `255.255.255.240` | 13                          | `10.3.1.206`       | `10.3.1.207`      |

### 2.Tableau d'adressage
| Nom machine          | Adresse IP `client1`          | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
| -------------------- | ----------------------------- | -------------------- | -------------------- | --------------------- |
| `router.tp3`         | `10.3.1.190/26`               | `10.3.1.126/25`      | `10.3.1.206/28`      | Carte NAT             |
| `dns1.server1.tp3`   | ...                           | `10.3.1.2/25`        | ...                  | `10.3.1.126/25`       |
| `web1.server2.tp3`   | ...                           | ...                  | `10.3.1.194/28`      | `10.3.1.206/28`       |
| `nfs1.server2.tp3`   | ...                           | ...                  | `10.3.1.195/28`      | `10.3.1.206/28`       |
| `dhcp.client1.tp3`   | `10.3.1.130/26`               | ...                  | ...                  | `10.3.1.190/26`       |
| `marcel.client1.tp3` | `10.3.1.131/26-10.3.1.189/26` | ...                  | ...                  | `10.3.1.190/26`       |
| `johnny.client1.tp3` | `10.3.1.131/26-10.3.1.189/26` | ...                  | ...                  | `10.3.1.190/26`       |

**fichiers conf du DNS**

[**named.conf**](files/named.conf)

[**server1.tp3.forward**](files/server1.tp3.forward)

[**server2.tp3.forward**](files/server2.tp3.forward)