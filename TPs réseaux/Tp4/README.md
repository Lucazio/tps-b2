# TP4:Vers un réseau d'entreprise
# Sommaire
- [TP4:Vers un réseau d'entreprise](#tp4vers-un-réseau-dentreprise)
- [Sommaire](#sommaire)
- [I.Dumb Switch](#idumb-switch)
  - [1. Adressage topologie 1](#1-adressage-topologie-1)
  - [2.Setup topologie 1](#2setup-topologie-1)
- [II.VLAN](#iivlan)
  - [1.Adressage topologie 2](#1adressage-topologie-2)
  - [2.Setup topologie 2](#2setup-topologie-2)
- [III.Routing](#iiirouting)
  - [1.Addressage de topologie 3](#1addressage-de-topologie-3)
  - [2.Setup topologie 3](#2setup-topologie-3)

# I.Dumb Switch
## 1. Adressage topologie 1

| Node  | IP            |
| ----- | ------------- |
| `pc1` | `10.1.1.1/24` |
| `pc2` | `10.1.1.2/24` |

## 2.Setup topologie 1
**Définir les ips et ping**
**sur `pc1`**
```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

PC1> save
Saving startup configuration to startup.vpc
.  done
```
**sur `pc2`**
```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0

PC2> save
Saving startup configuration to startup.vpc
.  done
```
**Tests**
```
PC1> ping 10.1.1.2 -c 4

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.176 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=3.331 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=5.185 ms
84 bytes from 10.1.1.2 icmp_seq=4 ttl=64 time=11.015 ms

PC2> ping 10.1.1.1 -c 4

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=8.437 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=8.456 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=10.524 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=8.727 ms
```
# II.VLAN
## 1.Adressage topologie 2
| Node  | IP            | VLAN |
| ----- | ------------- | ---- |
| `pc1` | `10.1.1.1/24` | 10   |
| `pc2` | `10.1.1.2/24` | 10   |
| `pc3` | `10.1.1.3/24` | 20   |
## 2.Setup topologie 2
**Sur `pc3`**
```
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0

PC3> save
Saving startup configuration to startup.vpc
.  done
```
**Tests des pings**
**`pc1`**
```
PC1> ping 10.1.1.2 -c 1

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.999 ms

PC1> ping 10.1.1.3 -c 1

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=17.369 ms
```
**`pc2`**
```
PC2> ping 10.1.1.1 -c 1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=11.316 ms

PC2> ping 10.1.1.3 -c 1

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=6.830 ms
```
**`pc3`**
```
PC3> ping 10.1.1.1 -c 1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=12.184 ms

PC3> ping 10.1.1.2 -c 1

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=9.694 ms
```
**Configuration des VLANS**
```
Switch#conf t
Switch(config)#vlan 10
Switch(config-vlan)#exit
Switch(config)#vlan 20
Switch(config-vlan)#exit
Switch(config)#exit
```
**ajout aux VLANs**
**`pc1`**
```
Switch#conf t
Switch(config)#interface gigabitEthernet0/1
Switch(config-if)#switchport mode acces
Switch(config-if)#switchport acces vlan 10
Switch(config-if)#exit
```
**`pc2`**
```
Switch(config)#interface gigabitEthernet0/2
Switch(config-if)#switchport mode acces
Switch(config-if)#switchport acces vlan 10
Switch(config-if)#exit
```
**`pc3`**
```
Switch(config)#interface gigabitEthernet0/3
Switch(config-if)#switchport mode acces
Switch(config-if)#switchport acces vlan 20
Switch(config-if)#exit
Switch(config)#exit
```
**verif**
```
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi1/0, Gi1/1, Gi1/2
                                                Gi1/3, Gi2/0, Gi2/1, Gi2/2
                                                Gi2/3, Gi3/0, Gi3/1, Gi3/2
                                                Gi3/3
10   VLAN0010                         active    Gi0/1, Gi0/2
20   VLAN0020                         active    Gi0/3
[...]
```
**sur `pc1`**
```
PC1> ping 10.1.1.2 -c 1

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=11.134 ms

PC1> ping 10.1.1.3 -c 1

host (10.1.1.3) not reachable
```
**sur `pc2`**
```
PC2> ping 10.1.1.1 -c 1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=9.540 ms

PC2> ping 10.1.1.3 -c 1

host (10.1.1.3) not reachable
```
**sur `pc3`**
```
PC3> ping 10.1.1.1 -c 1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2 -c 1

host (10.1.1.2) not reachable
```
# III.Routing
## 1.Addressage de topologie 3
Les réseaux et leurs VLANs associés :

| Réseau    | Adresse       | VLAN associé |
|-----------|---------------|--------------|
| `clients` | `10.1.1.0/24` | 11           |
| `admins`  | `10.2.2.0/24` | 12           |
| `servers` | `10.3.3.0/24` | 13           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`       | `admins`        | `servers`       |
|--------------------|-----------------|-----------------|-----------------|
| `pc1.clients.tp4`  | `10.1.1.1/24`   | x               | x               |
| `pc2.clients.tp4`  | `10.1.1.2/24`   | x               | x               |
| `adm1.admins.tp4`  | x               | `10.2.2.1/24`   | x               |
| `web1.servers.tp4` | x               | x               | `10.3.3.1/24`   |
| `r1`               | `10.1.1.254/24` | `10.2.2.254/24` | `10.3.3.254/24` |
## 2.Setup topologie 3
