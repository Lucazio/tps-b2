# TP2 : On va router des trucs
## I. ARP
### 1.Echange ARP
```
[lucas@node1 ~]$ ping 10.2.1.12 -c 4
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.369 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.384 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.334 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.571 ms

--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3095ms
rtt min/avg/max/mdev = 0.334/0.414/0.571/0.094 ms
```
```
[lucas@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr 08:00:27:0f:fe:1d REACHABLE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:4e REACHABLE
```
```
[lucas@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:23:85:73 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:4e DELAY
```
d'après les tables ARP l'adresse MAC de node1 est 08:00:27:23:85:73 et l'adresse de node2 est 08:00:27:0f:fe:1d
```
[lucas@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr 08:00:27:0f:fe:1d STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:4e DELAY
```
```
[lucas@node2 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0f:fe:1d brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe0f:fe1d/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
l'info est donc correcte, la MAC de node2 est bien 08:00:27:0f:fe:1d
### 2.Analyse de trames
```
[lucas@node1 ~]$ sudo ip n flush all
[sudo] password for lucas:
[lucas@node1 ~]$ ping 10.2.1.12 -c 4
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.750 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.490 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.443 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.400 ms

--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3104ms
rtt min/avg/max/mdev = 0.400/0.520/0.750/0.138 ms
```
```
[lucas@node1 ~]$ sudo ip n flush all
[lucas@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_arp.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C10 packets captured
10 packets received by filter
0 packets dropped by kernel
```
| ordre | type trame  | source                      | destination                 |
| ----- | ----------- | --------------------------- | --------------------------- |
| 1     | Requête ARP | `node1` `08:00:27:23:85:73` | Broadcast `FF:FF:FF:FF:FF`  |
| 2     | Réponse ARP | `node2` `08:00:27:0F:FE:1D` | `node1` `08:00:27:23:85:73` |
## II.Routage
les 3 VMs sont mises en place :
```
[lucas@router ~]$ hostname
router.net2.tp2
[lucas@router ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4f:cc:6a brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85092sec preferred_lft 85092sec
    inet6 fe80::a00:27ff:fe4f:cc6a/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d8:fb:a2 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fed8:fba2/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:6b:f7:da brd ff:ff:ff:ff:ff:ff
    inet 10.2.2.254/24 brd 10.2.2.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe6b:f7da/64 scope link
       valid_lft forever preferred_lft forever
```
```
[lucas@node1 ~]$ hostname
node1.net1.tp2
[lucas@node1 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:23:85:73 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe23:8573/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
```
[lucas@marcel ~]$ hostname
marcel.net2.tp2
[lucas@marcel ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a3:1a:ce brd ff:ff:ff:ff:ff:ff
    inet 10.2.2.12/24 brd 10.2.2.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fea3:1ace/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
### 1.Mise en place du routage
#### Activation du routage sur "router.net2.tp2"
```
[lucas@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8888/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[lucas@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[lucas@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[lucas@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
#### Ajout des routes statiques nécessaires:
```
[lucas@node1 ~]$ sudo ip route add 10.2.2.0/24 via 10.2.1.254 dev enp0s8
```
```
[lucas@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
```
```
[lucas@node1 ~]$ ip route show
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 100
10.2.2.0/24 via 10.2.1.254 dev enp0s8 proto static metric 100
```
```
[lucas@marcel ~]$ ip route show
10.2.1.0/24 via 10.2.2.254 dev enp0s8
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 100
```
vérifions si cela fonctionne:
```
[lucas@marcel ~]$ ping -c 4 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.77 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.62 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.69 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=63 time=1.83 ms

--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 1.617/1.724/1.827/0.094 ms
```
```
[lucas@marcel ~]$ ping -c 4 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=1.77 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.62 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.69 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=63 time=1.83 ms

--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 1.617/1.724/1.827/0.094 ms
```
On peut ping marcel depuis node1 et node1 depuis marcel.
### 2.Analyse de trames
Vider les tables arp
```
[lucas@node1 ~]$ sudo ip n flush all
```
```
[lucas@marcel ~]$ sudo ip n flush all
```
```
[lucas@router ~]$ sudo ip n flush all
```
Ping de node1 à marcel
```
[lucas@node1 ~]$ ping -c 4 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=2.04 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.68 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.74 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.74 ms

--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 1.675/1.796/2.037/0.141 ms
```
Tables arp
```
[lucas@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0e DELAY  <--- pas interessant
10.2.1.254 dev enp0s8 lladdr 08:00:27:d8:fb:a2 STALE
```
```
[lucas@marcel ~]$ ip n s
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:1f DELAY  <--- pas interessant
10.2.2.254 dev enp0s8 lladdr 08:00:27:6b:f7:da STALE
```
```
[lucas@router ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0e DELAY  <--- pas interessant
10.2.2.12 dev enp0s9 lladdr 08:00:27:a3:1a:ce STALE
10.2.1.11 dev enp0s8 lladdr 08:00:27:23:85:73 STALE
```
tout d'abord on peut ignorer 10.2.2.1 et 10.2.1.1 car ce sont les connexions en ssh qui les mettent dans la table ARP.
Le ping part de node1, il envoie une requête ARP que router intercepte parce qu'il (node1) ne sait pas où le joindre donc node1 met router dans sa table ARP et router met node1 dans sa table ARP.
Router envoie une requête ARP (dans le réseau 10.2.2.0/24) que marcel intercepte car router ne sait pas où joindre marcel.
Router met donc marcel dans sa table ARP et marcel met router dans sa table ARP.
#### Ping avec tcpdump
```
[lucas@node1 ~]$ sudo ip n flush all
```
```
[lucas@marcel ~]$ sudo ip n flush all
```
```
[lucas@router ~]$ sudo ip n flush all
```
```
[lucas@marcel ~]$ sudo tcpdump -i enp0s8 -w tp2_routage_marcel.pcap not port 22
```
```
[lucas@router ~]$ sudo tcpdump -i enp0s8 -w tp2_routage_router.pcap not port 22
```
```
[lucas@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_routage_node1.pcap not port 22
```

```
[lucas@node1 ~]$ ping 10.2.2.12 -c 1
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.02 ms

--- 10.2.2.12 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 1.021/1.021/1.021/0.000 ms
```
| ordre | type trame  | IP source             | MAC source                   | IP destination         | MAC destination              |
| ----- | ----------- | --------------------- | ---------------------------- | ---------------------- | ---------------------------- |
| 1     | Requête ARP | x                     | `node1` `08:00:27:23:85:73`  | x                      | Broadcast `FF:FF:FF:FF:FF`   |
| 2     | Réponse ARP | x                     | `router` `08:00:27:D8:FB:A2` | x                      | `node1` `08:00:27:23:85:73`  |
| 3     | Ping        | `node1` `10.2.1.11`   | `node1` `08:00:27:23:85:73`  | `marcel` `10.2.2.12`   | `router` `08:00:27:D8:FB:A2` |
| 4     | Requête ARP | x                     | `router` `08:00:27:6B:F7:DA` | x                      | Broadcast `FF:FF:FF:FF:FF`   |
| 5     | Réponse ARP | x                     | `marcel` `08:00:27:A3:1A:CE` | x                      | `router` `08:00:27:6B:F7:DA` |
| 6     | Ping        | `router` `10.2.2.254` | `router` `08:00:27:6B:F7:DA` | `marcel` `10.2.2.12`   | `marcel` `08:00:27:A3:1A:CE` |
| 7     | Pong        | `marcel` `10.2.2.12`  | `marcel` `08:00:27:A3:1A:CE` | `routeur` `10.2.2.254` | `router` `08:00:27:6B:F7:DA` |
| 8     | Pong        | `marcel` `10.2.2.12`  | `router` `08:00:27:D8:FB:A2` | `node1` `10.2.1.11`    | `node1` `08:00:27:23:85:73`  |
### 3.Accès internet
le routeur a un accès internet :
```
[lucas@router ~]$ ping -c 4 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=271 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=62.7 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=35.5 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=263 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 35.475/157.993/270.536/109.377 ms
```
on ajoute les routes
```
[lucas@node1 ~]$ sudo ip route add default via 10.2.1.254 dev enp0s8
[lucas@node1 ~]$ cat /etc/sysconfig/network
GATEWAY=10.2.1.254
[lucas@node1 ~]$ ping -c 4 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=43.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=353 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=19.8 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 19.773/109.301/353.295/141.208 ms
```
```
[lucas@marcel ~]$ sudo ip route add default via 10.2.2.254 dev enp0s8
[lucas@marcel ~]$ cat /etc/sysconfig/network
GATEWAY=10.2.2.254
[lucas@marcel ~]$ ping -c 4 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=115 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=21.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=20.1 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=54.8 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3003ms
rtt min/avg/max/mdev = 20.141/52.803/114.800/38.391 ms
```
```
[lucas@node1 ~]$ sudo cat /etc/resolv.conf
nameserver 1.1.1.1
[lucas@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 21192
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 106 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 23 19:35:48 CEST 2021
;; MSG SIZE  rcvd: 53

[lucas@node1 ~]$ ping -c4 google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=1 ttl=113 time=508 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=2 ttl=113 time=20.3 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=3 ttl=113 time=43.2 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=4 ttl=113 time=19.9 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3002ms
rtt min/avg/max/mdev = 19.876/147.928/508.313/208.282 ms
```
```
[lucas@marcel ~]$ sudo cat /etc/resolv.conf
nameserver 1.1.1.1
[lucas@marcel ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 25553
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; Query time: 160 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 23 19:36:20 CEST 2021
;; MSG SIZE  rcvd: 53

[lucas@marcel ~]$ ping -c 4 google.com
PING google.com (216.58.209.238) 56(84) bytes of data.
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=1 ttl=113 time=268 ms
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=2 ttl=113 time=19.4 ms
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=3 ttl=113 time=136 ms
64 bytes from par10s29-in-f14.1e100.net (216.58.209.238): icmp_seq=4 ttl=113 time=19.4 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 19.408/110.739/267.927/102.518 ms
```
```
[lucas@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_routage_internet.pcap not port 22
[lucas@node1 ~]$ ping -c1 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=216 ms

--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 215.786/215.786/215.786/0.000 ms
```


| ordre | type trame | IP source           | MAC source                   | IP destination      | MAC destination              |
| ----- | ---------- | ------------------- | ---------------------------- | ------------------- | ---------------------------- |
| 1     | ping       | `node1` `10.2.1.12` | `node1` `08:00:27:23:85:73`  | `8.8.8.8`           | `router` `08:00:27:D8:FB:A2` |
| 2     | pong       | `8.8.8.8`           | `router` `08:00:27:D8:FB:A2` | `node1` `10.2.1.12` | `node1` `08:00:27:23:85:73`  |
## III.DHCP
### Créer une machine node2
La machine node 2 a déjà été créée lors de la Partie I
### Installation du serveur sur node1
```
[lucas@node1 ~]$ sudo dnf install -y dhcp-server
[...]
[lucas@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;
option domain-name-servers 8.8.8.8;
subnet 10.2.1.0 netmask 255.255.255.0 {
        range 10.2.1.1 10.2.1.250;
        option routers 10.2.1.254;
}
[lucas@node1 ~]$ sudo systemctl enable --now dhcpd
[...]
```
maintenant il faut autoriser les connexions dhcp sur le pare feu
```
[lucas@node1 ~]$ sudo firewall-cmd --add-service=dhcp
success
[lucas@node1 ~]$ sudo firewall-cmd --runtime-to-permanent
success
```
La machine node2.net1.tp2 a déjà été créée à la partie I, sa configuration réseau pour avoir une adresse donnée par le serveur dhcp est la suivante:
```
[lucas@node2 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
[sudo] password for lucas:
BOOTPROTO=dhcp
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
```
le serveur dhcp lui a bien donné une adresse
```
[lucas@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0f:fe:1d brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.2/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 480sec preferred_lft 480sec
    inet6 fe80::a00:27ff:fe0f:fe1d/64 scope link
       valid_lft forever preferred_lft forever
```
```
[lucas@node1 ~]$ cat /var/lib/dhcpd/dhcpd.leases
[...]
lease 10.2.1.2 {
  starts 0 2021/09/26 17:53:57;
  ends 0 2021/09/26 18:03:57;
  cltt 0 2021/09/26 17:53:57;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:0f:fe:1d;
  uid "\001\010\000'\017\376\035";
  client-hostname "node2";
}
```
#### Améliorer la configuration du DHCP
la configuration actuelle du DHCP contient déjà les éléments d'amélioration:
```
[lucas@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
default-lease-time 600;
max-lease-time 7200;
authoritative;
option domain-name-servers 8.8.8.8;
subnet 10.2.1.0 netmask 255.255.255.0 {
        range 10.2.1.1 10.2.1.250;
        option routers 10.2.1.254;
}
```

#### Donner une nouvelle IP
```
[lucas@node2 ~]$ sudo dhclient
[lucas@node2 ~]$ sudo dhclient -r
Killed old client process
[lucas@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0f:fe:1d brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.5/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 387sec preferred_lft 387sec
    inet6 fe80::a00:27ff:fe0f:fe1d/64 scope link
       valid_lft forever preferred_lft forever

[lucas@node2 ~]$ ping -c 4 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.822 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.910 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=0.431 ms
64 bytes from 10.2.1.254: icmp_seq=4 ttl=64 time=0.992 ms

--- 10.2.1.254 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3091ms
rtt min/avg/max/mdev = 0.431/0.788/0.992/0.217 ms
```
#### Il doit avoir une route par défaut
```
[lucas@node2 ~]$ ip r s
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
[lucas@node2 ~]$ ping -c 4 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=19.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=21.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=112 time=19.10 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=112 time=20.4 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 19.708/20.379/21.478/0.699 ms
```
#### Il doit connaitre un serveur DNS
```
[lucas@node2 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 52659
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 512
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             94      IN      A       216.58.215.46

;; Query time: 18 msec
;; SERVER: 8.8.8.8#53(8.8.8.8)
;; WHEN: Sun Sep 26 21:09:15 CEST 2021
;; MSG SIZE  rcvd: 55

[lucas@node2 ~]$ ping -c 4 google.com
PING google.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=114 time=17.4 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=114 time=19.3 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=3 ttl=114 time=22.9 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=4 ttl=114 time=18.6 ms

--- google.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 17.411/19.530/22.886/2.046 ms
```
### 2.Analyse de trames
```
[lucas@node1 ~]$ sudo ip n flush all
[sudo] password for lucas:
[lucas@node1 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0e REACHABLE
```
```
[lucas@node2 ~]$ sudo ip n flush all
[sudo] password for lucas:
[lucas@node2 ~]$ ip n s
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:0e REACHABLE
```
```
[lucas@node2 ~]$ sudo tcpdump -i enp0s8 -w tp2_dhcp.pcap not port 22
[sudo] password for lucas:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C12 packets captured
12 packets received by filter
0 packets dropped by kernel
```
```
[lucas@node2 ~]$ sudo dhclient -r
Removed stale PID file
[lucas@node2 ~]$ sudo dhclient
```
| ordre | type trame    | IP source           | MAC source                  | IP destination      | MAC destination                 |
| ----- | ------------- | ------------------- | --------------------------- | ------------------- | ------------------------------- |
| 1     | ARP           | x                   | `node2` `08:00:27:0F:FE:1D` | x                   | `Broadcast` `FF:FF:FF:FF:FF:FF` |
| 2     | ARP           | x                   | `node1` `08:00:27:23:85:73` | x                   | `node2` `08:00:27:0F:FE:1D`     |
| 3     | DHCP Release  | `node2` `10.2.1.5`  | `node2` `08:00:27:0F:FE:1D` | `node1` `10.2.1.11` | `node1` `08:00:27:23:85:73`     |
| 4     | DHCP Discover | `0.0.0.0`           | `node2` `08:00:27:0F:FE:1D` | `255.255.255.255`   | `broadcast` `FF:FF:FF:FF:FF:FF` |
| 5     | ICMP (Ping?)  | `node1` `10.2.1.11` | `node1` `08:00:27:23:85:73` | `node2` `10.2.1.5`  | `node2` `08:00:27:0F:FE:1D`     |
| 6     | DHCP Offer    | `node1` `10.2.1.11` | `node1` `08:00:27:23:85:73` | `node2` `10.2.1.5`  | `node2` `08:00:27:0F:FE:1D`     |
| 7     | DHCP Request  | `0.0.0.0`           | `node2` `08:00:27:0F:FE:1D` | `255.255.255.255`   | `broadcast` `FF:FF:FF:FF:FF:FF` |
| 8     | DHCP ACK      | `node1` `10.2.1.11` | `node1` `08:00:27:23:85:73` | `node2` `10.2.1.5`  | `node2` `08:00:27:0F:FE:1D`     |
| 9     | ARP           | x                   | `node2` `08:00:27:0F:FE:1D` | x                   | `Broadcast` `FF:FF:FF:FF:FF:FF` |
| 10    | ARP           | x                   | `node2` `08:00:27:0F:FE:1D` | x                   | `Broadcast` `FF:FF:FF:FF:FF:FF` |
| 11    | ARP           | x                   | `node1` `08:00:27:23:85:73` | x                   | `node2` `08:00:27:0F:FE:1D`     |
| 12    | ARP           | x                   | `node2` `08:00:27:0F:FE:1D` | x                   | `node1` `08:00:27:23:85:73`     |