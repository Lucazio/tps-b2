# TP Réseau B2 2021
## I. Exploration locale en solo
### 1. Affichage d'informations sur la pile TCP/IP locale
#### infos  des cartes réseau du Pc :
```
PS C:\Users\hurlu> ipconfig /all
[...]
Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
   Adresse physique . . . . . . . . . . . : 24-4B-FE-65-96-30
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Description. . . . . . . . . . . . . . : Realtek 8821CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 70-66-55-8B-DF-07
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::59b9:617e:ffe:78de%21(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.16(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : lundi 13 septembre 2021 14:00:47
   Bail expirant. . . . . . . . . . . . . : lundi 13 septembre 2021 17:32:45
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   IAID DHCPv6 . . . . . . . . . . . : 208692821
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-67-10-0A-24-4B-FE-65-96-30
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
```
Pour l'interface WiFi:
le nom est Realtek 8821CE Wireless LAN 802.11ac PCI-E NIC, l'adresse MAC est 70-66-55-8B-DF-07 et l'ip est 10.33.2.16
Pour l'interface Ethernet:
le nom est Realtek PCIe GbE Family, l'adresse MAC est 24-4B-FE-65-96-30 et elle n'a pas d'adresse ip car elle n'est pas connectée au réseau.
#### gateway
```
PS C:\Users\hurlu> ipconfig
[...]
Carte réseau sans fil Wi-Fi :
[...]
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```
### comment afficher les informations sur une carte IP ?
Pour y arriver sur windows 10 j'ai suivi les étapes suivantes:
Panneau de configuration -> réseau et internet -> Centre réseau et partage -> modifier les paramètres de la carte
J'ai sélectioné la carte wifi puis statut -> détails
cela me montre les informations cherchées soit :
![](img/img1.png)
### à quoi sert la gateway dans le réseau ynov ?
la gateway dans le réseau d'ynov nous sert à pouvoir communiquer avec des réseaux exterieurs à celui d'ynov.

## 2. Modifications des informations
### A. modification de l'adresse IP (GUI)
On peut changer l'adresse IP en suivant globalement les mêmes étapes que pour l'afficher soit :
Panneau de configuration -> réseau et internet -> Centre réseau et partage -> modifier les paramètres de la carte
On sélectionne la bonne carte puis dans Proptiétés on sélectionne Protocole internet version 4(TCP/IPv4) -> propriétés, dans cette fenêtre nous pouvons changer d'adresse ip :
![](img/img2.png)
#### Pourquoi peut-on perdre l'accès à internet ?
Il se peut qu'en effectuant cette opération on perde l'accès à internet, cela signifie que l'adresse IP que nous voulions prendre est déjà utilisée, il faut donc en prendre une autre.
### B. Table ARP
```
PS C:\Users\hurlu> arp -a
Interface : 10.33.2.16 --- 0x15
  Adresse Internet      Adresse physique      Type
  10.33.0.98            48-a4-72-43-d7-76     dynamique
  10.33.2.56            c0-3c-59-a9-e0-75     dynamique
  10.33.2.236           48-a4-72-43-d7-76     dynamique
  10.33.3.185           3c-95-09-dc-a1-15     dynamique
  10.33.3.203           70-66-55-cf-4b-3b     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  [...]
```
l'adresse MAC de la passerelle est 00-12-00-40-4C-BF
On sait que cette adresse correspond à la passerelle du réseau car son ip est 10.33.3.253, on a vu précedemment que cette ip correspond à notre passerelle.

#### Remplissage de la table ARP

```
PS C:\Users\hurlu> ping 10.33.3.17
[...]
PS C:\Users\hurlu> ping 10.33.2.21
[...]
PS C:\Users\hurlu> ping 10.33.2.22
[...]
PS C:\Users\hurlu> arp -a
[...]
 10.33.2.21            f6-53-a2-fe-e7-15     dynamique
 10.33.2.22            f8-5e-a0-52-4f-36     dynamique
[...]
 10.33.3.17            80-32-53-e2-78-04     dynamique
 ```
 Les adresses MAC des machines que j'ai ping sont f6-53-a2-fe-e7-15 (10.33.2.21), f8-5e-a0-52-4f-36 (10.33.2.22) et 80-32-53-e2-78-04 (10.33.3.17)
 
 ### C. nmap
 après avoir repris une IP automatique j'ai utilisé la commande suivante:
 ```
 PS C:\Users\hurlu> nmap -sP 10.33.0.0/22
 [...]
 PS C:\Users\hurlu> Get-NetNeighbor
 [...]
 21      10.33.3.255                                        FF-FF-FF-FF-FF-FF     Permanent   ActiveStore
21      10.33.3.254                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.253                                        00-12-00-40-4C-BF     Reachable   ActiveStore
21      10.33.3.252                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.251                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.250                                        14-F6-D8-43-45-95     Stale       ActiveStore
21      10.33.3.249                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.248                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.247                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.246                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.245                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.244                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.243                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.242                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.241                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.240                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.239                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.238                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.237                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.236                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.235                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.234                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.233                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.232                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.231                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.230                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.229                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.228                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.227                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.226                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.225                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.224                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.223                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.222                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.221                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.220                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.219                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.218                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.217                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.216                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.215                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.214                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.213                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.212                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.211                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.210                                        D8-3B-BF-1A-7A-5C     Stale       ActiveStore
21      10.33.3.209                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.208                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.207                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.206                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.205                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.204                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.203                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.202                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.201                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.200                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.199                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.198                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.197                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.196                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.195                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.194                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.193                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.192                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.191                                        00-00-00-00-00-00     Unreachable ActiveStore
21      10.33.3.190                                        00-00-00-00-00-00     Unreachable ActiveStore
[...]
```
Comme on peut le voir la table est bien plus complète: on sait quelles ip sont disponibles (adresse MAC 00-00-00-00-00-00)

### D. Modification d'adresse IP
```
PS C:\Users\hurlu> nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 15:02 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.10
Host is up (0.64s latency).
MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)
Nmap scan report for 10.33.0.13
Host is up (0.13s latency).
MAC Address: D2:50:0E:26:94:8B (Unknown)
Nmap scan report for 10.33.0.21
Host is up (0.011s latency).
MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)
Nmap scan report for 10.33.0.27
Host is up (0.0070s latency).
MAC Address: A8:64:F1:8B:1D:4D (Intel Corporate)
Nmap scan report for 10.33.0.31
Host is up (0.044s latency).
MAC Address: FA:C5:6D:60:4B:4C (Unknown)
[...]
Nmap scan report for 10.33.3.253
Host is up (0.027s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.0050s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.2.16
Host is up.
Nmap done: 1024 IP addresses (142 hosts up) scanned in 33.44 seconds
```
d'après la table ARP montrée précedement l'adresse IP 10.33.3.190 serait libre c'est donc l'ip que l'on va utiliser (screen3), une fois les modifications appliquées on va vérifier qu'elles soient bien appliquées
```
PS C:\Users\hurlu> ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Description. . . . . . . . . . . . . . : Realtek 8821CE Wireless LAN 802.11ac PCI-E NIC
   Adresse physique . . . . . . . . . . . : 70-66-55-8B-DF-07
   DHCP activé. . . . . . . . . . . . . . : Non
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::59b9:617e:ffe:78de%21(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.190(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   IAID DHCPv6 . . . . . . . . . . . : 208692821
   DUID de client DHCPv6. . . . . . . . : 00-01-00-01-26-67-10-0A-24-4B-FE-65-96-30
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé
[...]
PS C:\Users\hurlu> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=17 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=22 ms TTL=58
Réponse de 1.1.1.1 : octets=32 temps=18 ms TTL=58

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 22ms, Moyenne = 18ms
```
Les modifications sont bien effectives, on a l'ip que l'on voulait et un accès internet
## II. Exploration locale en duo
### 3. Modification d'adresse IP
![](img/img4.png)![](img/img5.png)

Ma machine a l'ip 192.168.1.1 et celle de mon équipier est 192.168.1.2
```
PS C:\Users\hurlu> ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::1c24:5100:56eb:1cae%9
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.1.2
```
```
PS C:\Users\hurlu> ping 192.168.1.2

Envoi d’une requête 'Ping'  192.168.1.2 avec 32 octets de données :
Réponse de 192.168.1.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.1.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 1ms, Moyenne = 1ms
```
les machines peuvent bien communiquer ensemble
```
PS C:\Users\hurlu> arp -a
[...]
Interface : 192.168.1.1 --- 0x9
  Adresse Internet      Adresse physique      Type
  192.168.1.2           7c-8a-e1-43-30-b5     dynamique
```
### 4. Utilisation d'un des deux comme gateway
Pour cette étape ma machine sera le gateway (192.168.1.1)
```
PS C:\WINDOWS\system32> ping 192.168.1.1

Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.1.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 2ms
```
l'autre machine peut bien communiquer avec la mienne
```
PS C:\WINDOWS\system32> ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::5dc0:10f3:5fb4:acb2%7
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.1.1
```
sa passerelle est bien ma machine
Pour partager la connexion nous avons suivi les étapes suivantes: 
Panneau de configuration -> Réseau et internet -> Centre réseau et partage -> modifier les paramètres de la carte -> on sélectionne la bonne carte (WiFi dans notre cas) -> Propriétés -> Partage 

![](img/img6.png)
```
PS C:\WINDOWS\system32> ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=35 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=30 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=73 ms TTL=114
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=114

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 19ms, Maximum = 73ms, Moyenne = 39ms
PS C:\WINDOWS\system32> tracert 8.8.8.8

Détermination de l’itinéraire vers dns.google [8.8.8.8]
avec un maximum de 30 sauts :

  1    <1 ms     *        2 ms  LAPTOP-4DFELSLI [192.168.1.1]
  [...]
```
sa machine passe bien par la mienne pour communiquer avec 8.8.8.8
### 5. Petit chat privé
pour cette partie ma machine sera la serveur
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> ./nc64.exe -l -p 8888
dnbgkjdngkjndjkhnjth
allo
ici la voix
non
pas ça
```
du côté client ça donne ça:
```
PS C:\Users\Paula\Downloads\netcat-win32-1.11\netcat-1.11> ./nc64.exe 192.168.1.1 8888
dnbgkjdngkjndjkhnjth
allo
ici la voix
non
pas ça
```
#### Pour aller plus loin:
ici je suis encore le serveur
avec la premiere commande j'accepte uniquement les connexions venant de 192.168.1.1 (je suis 192.168.1.1 donc ça ne devrait pas marcher)
avec la deuxieme j'accepte uniquement celles venant de 192.168.1.2 étant donné que c'est l'adresse de la deuxieme machine ça fonctionne.
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> ./nc64.exe -l -p 8888 192.168.1.1
invalid connection to [192.168.1.1] from (UNKNOWN) [192.168.1.2] 32525
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> ./nc64.exe -l -p 8888 192.168.1.2
ok
test
coucou
```
du côté client ça donne ça :
```
PS C:\Users\Paula\Downloads\netcat-win32-1.11\netcat-1.11> ./nc64.exe 192.168.1.1 8888
PS C:\Users\Paula\Downloads\netcat-win32-1.11\netcat-1.11> ./nc64.exe 192.168.1.1 8888
ok
test
coucou
```
### 6. Firewall
nous avons réactivé le firewall, les ping ne passent donc plus
```
PS C:\Users\hurlu> ping 192.168.1.2

Envoi d’une requête 'Ping'  192.168.1.2 avec 32 octets de données :
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.
Délai d’attente de la demande dépassé.

Statistiques Ping pour 192.168.1.2:
    Paquets : envoyés = 4, reçus = 0, perdus = 4 (perte 100%),
```
pour accepter les pings nous avons fait la manipulation suivante sur les deux machines:
pare-feu windows defender -> Paramètres avancés -> Règles du trafic entrant -> ajouter une nouvelle règle -> Personalisée -> Tous les programmes -> type de protocole ICMP v4 -> suivant jusqu'au nom, on met un nom puis terminer
```
PS C:\Users\hurlu> ping 192.168.1.2

Envoi d’une requête 'Ping'  192.168.1.2 avec 32 octets de données :
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.1.2 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.1.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```
les pings fonctionnent désormais.
Pour utiliser netcat avec le firewall activé nous allons utiliser le port 1052
sur la machine qui servira de serveur nous allons créer 2 nouvelles règles de trafic entrant
même démarche que pour accepter les pings sauf que cette fois on en fait 2, une pour le protocole TCP sur le port 1052 et l'autre pour le protocole UDP sur le port 1052
du côté serveur ça donne ça :
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 1052
test
bonjour
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 1054
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11>
```
Pour la deuxieme commande cela ne fonctionnait pas car le port 1054 n'est pas ouvert mais seulement le port 1052
## III. Manipulations d'autres outils/protocoles côté  client
### 1. DHCP
```
PS C:\Users\hurlu> ipconfig /all
[...]
Carte réseau sans fil Wi-Fi :
[...]
   DHCP activé. . . . . . . . . . . . . . : Oui
   Configuration automatique activée. . . : Oui
   Adresse IPv6 de liaison locale. . . . .: fe80::59b9:617e:ffe:78de%21(préféré)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.16(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Bail obtenu. . . . . . . . . . . . . . : jeudi 16 septembre 2021 15:45:01
   Bail expirant. . . . . . . . . . . . . : jeudi 16 septembre 2021 17:45:01
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
   ```
Je suis connecté au réseau d'ynov, l'adresse du serveur DHCP est donc 10.33.3.254 comme affiché ci-dessus, le bail DHCP expire le jeudi 16 septembre 2021 à 17h45.
### 2. DNS
```
PS C:\Users\hurlu> ipconfig /all
[...]
   Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
                                       8.8.4.4
```
L'adresse du serveur DNS que connait mon ordinateur est 8.8.8.8
```
PS C:\Users\hurlu> nslookup google.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:81a::200e
          216.58.213.174

PS C:\Users\hurlu> nslookup ynov.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```
j'ai interrogé le serveur DNS 8.8.8.8(dns de google) pour savoir quelles étaient les ip correspondantes à "google.com" et "ynov.com", l'ip de google est 216.58.213.174 et celle d'ynov est 92.243.16.143
```
PS C:\Users\hurlu> nslookup 78.74.21.21
Serveur :   dns.google
Address:  8.8.8.8

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21

PS C:\Users\hurlu> nslookup 92.146.54.88
Serveur :   dns.google
Address:  8.8.8.8

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
j'ai demandé au serveur DNS 8.8.8.8 si il savait les noms correspondants aux ips 78.74.21.21 et 92.146.54.88, on m'a répondu que les noms correspondants sont : host-78-74-21-21.homerun.telia.com et apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
## IV. Wireshark
Pour cette partie je ne suis plus sur le réseau d'ynov,l'adresse de ma passerelle est donc 192.168.1.1, l'adresse du réseau entre les deux cartes ethernet a été modifié en 192.168.137.0/30 pour ne pas créer de confusions
### Ping
```
PS C:\Users\hurlu> ping 192.168.1.1

Envoi d’une requête 'Ping'  192.168.1.1 avec 32 octets de données :
Réponse de 192.168.1.1 : octets=32 temps=3 ms TTL=64
Réponse de 192.168.1.1 : octets=32 temps=3 ms TTL=64
Réponse de 192.168.1.1 : octets=32 temps=3 ms TTL=64
Réponse de 192.168.1.1 : octets=32 temps=4 ms TTL=64

Statistiques Ping pour 192.168.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 3ms, Maximum = 4ms, Moyenne = 3ms
```
![](img/img7.png)
voici les trames du ping, on peut y voir chaque étape du ping, une série de 4 demandes/réponses
### Netcat
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> .\nc.exe -l -p 1052
bonjour
salut
???
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11>
```
![](img/img8.png)
voici les trames du netcat, on peut y discerner un bout de message "bonjour"
### DNS
```
PS C:\Users\hurlu\Desktop\netcat-win32-1.11\netcat-1.11> nslookup google.com
Serveur :   dns.google
Address:  8.8.8.8

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:808::200e
          142.250.74.238
```
![](img/img9.png)
voici les trames de la requête dns, à droite on voit beaucoup de 8.8.8.8, c'est l'adresse du serveur dns à qui l'on pose une question