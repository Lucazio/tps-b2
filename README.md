# TP-B2
## là où se trouveront tous mes TP de B2, on peut y accéder en suivant ces liens:
- [TP Réseaux](TPs%20réseaux/README.md)
  - [TP1](TPs%20réseaux/Tp1/README.md) 
  - [TP2](TPs%20réseaux/Tp2/README.md)
  - [TP3](TPs%20réseaux/Tp3/README.md)  
- [TP Linux](TPs%20linux/README.md)
  - [TP1](TPs%20linux/TP1/README.md)
  - [TP2](TPs%20linux/TP2/README.md)
    - [Part1](TPs%20linux/TP2/Part1/README.md)
    - [Part2](TPs%20linux/TP2/Part2/README.md)