# TP2 pt. 1 : Gestion de service

# Sommaire

- [TP2 pt. 1 : Gestion de service](#tp2-pt-1--gestion-de-service)
- [Sommaire](#sommaire)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro](#1-intro)
  - [2. Setup](#2-setup)
    - [A. Serveur Web et NextCloud](#a)
    - [B. Base de données](#b-base-de-données)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# I. Un premier serveur web

## 1. Installation
**Installer le serveur Apache**
```
[lucas@web ~]$ sudo dnf install httpd
[sudo] password for lucas:
[...]
Complete!
```

**Démarer le service Apache**
```
[lucas@web ~]$ sudo systemctl start httpd
[lucas@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[lucas@web ~]$ ss -alnpt
State                       Recv-Q                      Send-Q                                            Local Address:Port                                             Peer Address:Port                      Process
LISTEN                      0                           128                                                     0.0.0.0:22                                                    0.0.0.0:*
LISTEN                      0                           128                                                           *:80                                                          *:*
LISTEN                      0                           128                                                        [::]:22                                                       [::]:*
[lucas@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
[sudo] password for lucas:
success
[lucas@web ~]$ sudo firewall-cmd --reload
success
[lucas@web ~]$ sudo systemctl daemon-reload

```
**TEST**
```
[lucas@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-09-29 17:06:27 CEST; 18min ago
     Docs: man:httpd.service(8)
 Main PID: 24009 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 2732)
   Memory: 16.3M
   CGroup: /system.slice/httpd.service
           ├─24009 /usr/sbin/httpd -DFOREGROUND
           ├─24010 /usr/sbin/httpd -DFOREGROUND
           ├─24011 /usr/sbin/httpd -DFOREGROUND
           ├─24012 /usr/sbin/httpd -DFOREGROUND
           └─24013 /usr/sbin/httpd -DFOREGROUND

Sep 29 17:06:27 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Sep 29 17:06:27 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Sep 29 17:06:27 web.tp2.linux httpd[24009]: Server configured, listening on: port 80
```
Le service est démarré:
```
Active: active (running) since Wed 2021-09-29 17:06:27 CEST; 18min ago
```
Le service se démarre au démarrage:
```
Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
```
On peut joindre le serveur localement:
```
[lucas@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
  background: -moz-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%)  ;
  background: -webkit-linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%) ;
  background: linear-gradient(180deg, rgba(20,72,50,1) 30%, rgba(0,0,0,1) 90%);
  background-repeat: no-repeat;
  background-attachment: fixed;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="#3c6eb4",endColorstr="#3c95b4",GradientType=1);
        color: white;
        font-size: 0.9em;
        font-weight: 400;
        font-family: 'Montserrat', sans-serif;
        margin: 0;
        padding: 10em 6em 10em 6em;
        box-sizing: border-box;

      }


  h1 {
    text-align: center;
    margin: 0;
    padding: 0.6em 2em 0.4em;
    color: #fff;
    font-weight: bold;
    font-family: 'Montserrat', sans-serif;
    font-size: 2em;
  }
  h1 strong {
    font-weight: bolder;
    font-family: 'Montserrat', sans-serif;
  }
  h2 {
    font-size: 1.5em;
    font-weight:bold;
  }

  .title {
    border: 1px solid black;
    font-weight: bold;
    position: relative;
    float: right;
    width: 150px;
    text-align: center;
    padding: 10px 0 10px 0;
    margin-top: 0;
  }

  .description {
    padding: 45px 10px 5px 10px;
    clear: right;
    padding: 15px;
  }

  .section {
    padding-left: 3%;
   margin-bottom: 10px;
  }

  img {

    padding: 2px;
    margin: 2px;
  }
  a:hover img {
    padding: 2px;
    margin: 2px;
  }

  :link {
    color: rgb(199, 252, 77);
    text-shadow:
  }
  :visited {
    color: rgb(122, 206, 255);
  }
  a:hover {
    color: rgb(16, 44, 122);
  }
  .row {
    width: 100%;
    padding: 0 10px 0 10px;
  }

  footer {
    padding-top: 6em;
    margin-bottom: 6em;
    text-align: center;
    font-size: xx-small;
    overflow:hidden;
    clear: both;
  }

  .summary {
    font-size: 140%;
    text-align: center;
  }

  #rocky-poweredby img {
    margin-left: -10px;
  }

  #logos img {
    vertical-align: top;
  }

  /* Desktop  View Options */

  @media (min-width: 768px)  {

    body {
      padding: 10em 20% !important;
    }

    .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6,
    .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
      float: left;
    }

    .col-md-1 {
      width: 8.33%;
    }
    .col-md-2 {
      width: 16.66%;
    }
    .col-md-3 {
      width: 25%;
    }
    .col-md-4 {
      width: 33%;
    }
    .col-md-5 {
      width: 41.66%;
    }
    .col-md-6 {
      border-left:3px ;
      width: 50%;


    }
    .col-md-7 {
      width: 58.33%;
    }
    .col-md-8 {
      width: 66.66%;
    }
    .col-md-9 {
      width: 74.99%;
    }
    .col-md-10 {
      width: 83.33%;
    }
    .col-md-11 {
      width: 91.66%;
    }
    .col-md-12 {
      width: 100%;
    }
  }

  /* Mobile View Options */
  @media (max-width: 767px) {
    .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6,
    .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
      float: left;
    }

    .col-sm-1 {
      width: 8.33%;
    }
    .col-sm-2 {
      width: 16.66%;
    }
    .col-sm-3 {
      width: 25%;
    }
    .col-sm-4 {
      width: 33%;
    }
    .col-sm-5 {
      width: 41.66%;
    }
    .col-sm-6 {
      width: 50%;
    }
    .col-sm-7 {
      width: 58.33%;
    }
    .col-sm-8 {
      width: 66.66%;
    }
    .col-sm-9 {
      width: 74.99%;
    }
    .col-sm-10 {
      width: 83.33%;
    }
    .col-sm-11 {
      width: 91.66%;
    }
    .col-sm-12 {
      width: 100%;
    }
    h1 {
      padding: 0 !important;
    }
  }


  </style>
  </head>
  <body>
    <h1>HTTP Server <strong>Test Page</strong></h1>

    <div class='row'>

      <div class='col-sm-12 col-md-6 col-md-6 '></div>
          <p class="summary">This page is used to test the proper operation of
            an HTTP server after it has been installed on a Rocky Linux system.
            If you can read this page, it means that the software it working
            correctly.</p>
      </div>

      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>


        <div class='section'>
          <h2>Just visiting?</h2>

          <p>This website you are visiting is either experiencing problems or
          could be going through maintenance.</p>

          <p>If you would like the let the administrators of this website know
          that you've seen this page instead of the page you've expected, you
          should send them an email. In general, mail sent to the name
          "webmaster" and directed to the website's domain should reach the
          appropriate person.</p>

          <p>The most common email address to send to is:
          <strong>"webmaster@example.com"</strong></p>

          <h2>Note:</h2>
          <p>The Rocky Linux distribution is a stable and reproduceable platform
          based on the sources of Red Hat Enterprise Linux (RHEL). With this in
          mind, please understand that:

        <ul>
          <li>Neither the <strong>Rocky Linux Project</strong> nor the
          <strong>Rocky Enterprise Software Foundation</strong> have anything to
          do with this website or its content.</li>
          <li>The Rocky Linux Project nor the <strong>RESF</strong> have
          "hacked" this webserver: This test page is included with the
          distribution.</li>
        </ul>
        <p>For more information about Rocky Linux, please visit the
          <a href="https://rockylinux.org/"><strong>Rocky Linux
          website</strong></a>.
        </p>
        </div>
      </div>
      <div class='col-sm-12 col-md-6 col-md-6 col-md-offset-12'>
        <div class='section'>

          <h2>I am the admin, what do I do?</h2>

        <p>You may now add content to the webroot directory for your
        software.</p>

        <p><strong>For systems using the
        <a href="https://httpd.apache.org/">Apache Webserver</strong></a>:
        You can add content to the directory <code>/var/www/html/</code>.
        Until you do so, people visiting your website will see this page. If
        you would like this page to not be shown, follow the instructions in:
        <code>/etc/httpd/conf.d/welcome.conf</code>.</p>

        <p><strong>For systems using
        <a href="https://nginx.org">Nginx</strong></a>:
        You can add your content in a location of your
        choice and edit the <code>root</code> configuration directive
        in <code>/etc/nginx/nginx.conf</code>.</p>

        <div id="logos">
          <a href="https://rockylinux.org/" id="rocky-poweredby"><img src= "icons/poweredby.png" alt="[ Powered by Rocky Linux ]" /></a> <!-- Rocky -->
          <img src="poweredby.png" /> <!-- webserver -->
        </div>
      </div>
      </div>

      <footer class="col-sm-12">
      <a href="https://apache.org">Apache&trade;</a> is a registered trademark of <a href="https://apache.org">the Apache Software Foundation</a> in the United States and/or other countries.<br />
      <a href="https://nginx.org">NGINX&trade;</a> is a registered trademark of <a href="https://">F5 Networks, Inc.</a>.
      </footer>

  </body>
</html>
```
**Le service apache:**
**commande permettant d'activer le démarage automatique du service:**
```
[lucas@web ~]$ sudo systemctl enable httpd.service
```
**prouver avec une commande que le service apache se démarre automatiquement**
```
[lucas@web ~]$ sudo systemctl status httpd.service
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
[...]
```
Le `enabled;`montre que le service se lance au démarrage
**mettre en évidence la ligne où se trouve l'utilisateur utilisé pour le service apache**
```
[lucas@web ~]$ cat /etc/httpd/conf/httpd.conf
[...]
User apache
[...]
```
**confirmer qu'apache tourne avec l'utilisateur mentionné**
```
[lucas@web ~]$ ps -ef | grep httpd
root         832       1  0 13:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       856     832  0 13:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       857     832  0 13:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       858     832  0 13:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       859     832  0 13:41 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
lucas       1318    1287  0 13:44 pts/0    00:00:00 grep --color=auto httpd
```
Apache tourne bien avec l'utilisateur mentionné sauf le premier qui est root ???

**tout le contenu de la page apache est accessible à l'utilisateur mentionné**
```
[lucas@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Sep 29 16:50 .
drwxr-xr-x. 91 root root 4096 Sep 29 16:50 ..
-rw-r--r--.  1 root root 7621 Jun 11 17:23 index.html
```
**Changer l'utilisateur utilisé par apache**
**créer le nouvel utilisateur**
```
[lucas@web ~]$ sudo useradd newapache -b /usr/share/httpd -s /sbin/nologin
[lucas@web ~]$ sudo usermod -aG apache newapache
```
**modifier la configuration d'apache pour qu'il utilise `newapache`**
```
[lucas@web ~]$ cat /etc/httpd/conf/httpd.conf
[...]
User newapache
Group apache
[...]
```
**redémarer apache**
```
[lucas@web ~]$ sudo systemctl restart httpd.service
```
**vérifier que le changement a pris effet**
```
[lucas@web ~]$ ps -ef | grep httpd
root        1889       1  0 15:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+    1891    1889  0 15:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+    1892    1889  0 15:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+    1893    1889  0 15:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
newapac+    1894    1889  0 15:01 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
lucas       2115    1287  0 15:03 pts/0    00:00:00 grep --color=auto httpd
```
**Changer le port sur lequel écoute apache**
**modifier le fichier conf**
```
[lucas@web ~]$ cat /etc/httpd/conf/httpd.conf
[...]
Listen 2222
[...]
```
**ouvrir ce port firewall et fermer l'ancien**
```
[lucas@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[lucas@web ~]$ sudo firewall-cmd --add-port=2222/tcp --permanent
success
[lucas@web ~]$ sudo firewall-cmd --reload
success
[lucas@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: ssh
  ports: 2222/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
**redémarrer apache**
```
[lucas@web ~]$ sudo systemctl restart httpd.service
```
**prouver qu'apache tourne sur le bon port**
```
[lucas@web ~]$ ss -lnt
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             128                        0.0.0.0:22                      0.0.0.0:*
LISTEN        0             128                              *:2222                          *:*
LISTEN        0             128                           [::]:22                         [::]:*
```
**vérifier avec curl que l'on peut joindre le serveur en local**
```
[lucas@web ~]$ curl localhost:2222
<!doctype html>
<html>
[...]
</html>
```
# II.Une stack web plus avancée
## 2.Setup
### A.Serveur web et nextcloud
**install du serveur web**
**la conf est réinitialisée**
```
[lucas@web ~]$ sudo firewall-cmd --list-all
[...]
  ports: 80/tcp
[...]
[lucas@web ~]$ cat /etc/httpd/conf/httpd.conf
[...]
Listen 80
[...]
User apache
[...]
```
**install sur `web.tp2.linux`**
```
[lucas@web ~]$ sudo dnf install -y epel-release
[...]
[lucas@web ~]$ sudo dnf update
[...]
[lucas@web ~]$ sudo dnf install -y https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[...]
[lucas@web ~]$ sudo dnf module list php
[...]
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                           3.1 MB/s | 2.0 MB     00:00
Rocky Linux 8 - AppStream
Name               Stream                 Profiles                                Summary
php                7.2 [d]                common [d], devel, minimal              PHP scripting language
php                7.3                    common [d], devel, minimal              PHP scripting language
php                7.4                    common [d], devel, minimal              PHP scripting language
[...]
[lucas@web ~]$ sudo dnf module enable php:remi-7.4 -y     
[...]  
[lucas@web ~]$ sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
[...]
[lucas@web ~]$ sudo mkdir /etc/httpd/sites-available
[lucas@web ~]$ sudo mkdir /etc/httpd/sites-enabled
[lucas@web ~]$ sudo mkdir /var/www/sub-domains
[lucas@web ~]$ sudo vim /etc/httpd/conf/httpd.conf
[lucas@web ~]$ cat /etc/httpd/conf/httpd.conf
[...]
Include /etc/httpd/sites-enabled
[lucas@web ~]$ sudo vim /etc/httpd/sites-available/linux.tp2.nextcloud
[lucas@web ~]$ sudo cat /etc/httpd/sites-available/linux.tp2.nextcloud
<VirtualHost *:80>
  DocumentRoot /var/www/sub-domains/linux.tp2.nextcloud/html/
  ServerName  nextcloud.tp2.linux

  <Directory /var/www/sub-domains/linux.tp2.nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
[lucas@web ~]$ sudo ln -s /etc/httpd/sites-available/linux.tp2.nextcloud /etc/httpd/sites-enabled/
[lucas@web ~]$ sudo mkdir -p /var/www/sub-domains/linux.tp2.nextcloud/html
[lucas@web ~]$ timedatectl
               Local time: Sun 2021-10-10 20:22:16 CEST
           Universal time: Sun 2021-10-10 18:22:16 UTC
                 RTC time: Sun 2021-10-10 18:22:16
                Time zone: Europe/Paris (CEST, +0200)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no
[lucas@web ~]$ sudo vim /etc/opt/remi/php74/php.ini
[lucas@web ~]$ cat /etc/opt/remi/php74/php.ini
[...]
date.timezone = "Europe/Paris"
[...]
[lucas@web ~]$ wget https://download.nextcloud.com/server/releases/nextcloud-22.2.0.zip
[...]
[lucas@web ~]$ unzip nextcloud-22.2.0.zip
[...]
[lucas@web ~]$ cd nextcloud/
[lucas@web nextcloud]$ sudo mv * /var/www/sub-domains/linux.tp2.nextcloud/html/
[lucas@web nextcloud]$ cd
[lucas@web ~]$ sudo chown -Rf apache.apache /var/www/sub-domains/linux.tp2.nextcloud/html/
[lucas@web ~]$ sudo systemctl restart httpd
```
**Je peux acceder à la page d'acceuil de nextcloud, on va donc passer à l'install de `db.tp2.linux`**
### B. Base de données
**Install de `db.tp2.linux`**
```
[lucas@db ~]$ sudo dnf install mariadb-server
[...]
[lucas@db ~]$ sudo systemctl enable mariadb
[...]
[lucas@db ~]$ sudo systemctl start mariadb.service
[lucas@db ~]$ sudo mysql_secure_installation
[...]
All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
[lucas@db ~]$ sudo ss -atpnl
State                    Recv-Q                   Send-Q                                     Local Address:Port                                     Peer Address:Port                  Process
LISTEN                   0                        128                                              0.0.0.0:22                                            0.0.0.0:*                      users:(("sshd",pid=831,fd=5))
LISTEN                   0                        80                                                     *:3306                                                *:*                      users:(("mysqld",pid=26108,fd=21))
LISTEN                   0                        128                                                 [::]:22                                               [::]:*                      users:(("sshd",pid=831,fd=7))
[lucas@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[lucas@db ~]$ sudo firewall-cmd --reload
success
```
**on voit que le port utilisé par mysqld est le port 3306**
**Préparation de la base pour NextCloud**
```
[lucas@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 15
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```
**Exploration de la base de données**
```
[lucas@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 29
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
| root      |
+-----------+
5 rows in set (0.000 sec)
```
### C. Finaliser l'installation de NextCloud
**Modifier le fichier `hosts`**
```
PS C:\Users\hurlu> code 'C:\Windows\System32\drivers\etc\hosts'
PS C:\Users\hurlu> cat 'C:\Windows\System32\drivers\etc\hosts'
#
10.102.1.11 web.tp2.linux
127.0.0.1 localhost
::1 localhost
```
**visiter Nextcloud**
```
PS C:\Users\hurlu> curl web.tp2.linux


StatusCode        : 200
StatusDescription : OK
Content           : <!DOCTYPE html>
                    <html>
                    <head>
                        <script> window.location.href="index.php"; </script>
                        <meta http-equiv="refresh" content="0; URL=index.php">
                    </head>
                    </html>

RawContent        : HTTP/1.1 200 OK
                    Keep-Alive: timeout=5, max=100
                    Connection: Keep-Alive
                    Accept-Ranges: bytes
                    Content-Length: 156
                    Content-Type: text/html; charset=UTF-8
                    Date: Sun, 10 Oct 2021 20:29:32 GMT
                    ETag: "...
Forms             : {}
Headers           : {[Keep-Alive, timeout=5, max=100], [Connection, Keep-Alive], [Accept-Ranges, bytes], [Content-Length, 156]...}
Images            : {}
InputFields       : {}
Links             : {}
ParsedHtml        : mshtml.HTMLDocumentClass
RawContentLength  : 156
```
**Exploration de base de données**
**déterminer le nombre de tables ajoutées lors de la finalisation de l'installation**
```
MariaDB [nextcloud]> select table_schema as database_name,
    ->     count(*) as tables
    -> from information_schema.tables
    -> where table_type = 'BASE TABLE'
    ->       and table_schema not in ('information_schema', 'sys',
    ->                                'performance_schema', 'mysql')
    -> group by table_schema
    -> order by table_schema;
+---------------+--------+
| database_name | tables |
+---------------+--------+
| nextcloud     |     87 |
+---------------+--------+
1 row in set (0.002 sec)
```
**Tableau machines**
| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             |`22/tcp` `80/tcp`| Toutes ?   |
|`db.tp2.linux`   |`10.102.1.12`  | Serveur Base de données |`22/tcp` `3306/tcp`| `10.102.1.11`|
