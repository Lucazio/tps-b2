# TP2 pt. 2 : Maintien en condition opérationnelle
| Machine         | IP            | Service                 | Port ouvert | IPs autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | ?           | ?             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données | ?           | ?             |
## I. Monitoring
### 2. Setup
**Setup Netdata**
**Sur `web.tp2.linux`**
```
[lucas@web ~]$ sudo su -
[...]
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
[...]
[root@web ~]# exit
logout
```
**Sur `db.tp2.linx`**
```
[lucas@db ~]$ sudo su -
[...]
[root@db ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
[root@db ~]# exit
logout
```
**Manipulation du service netdata**
**Sur `web.tp2.linux`**
```
[lucas@web ~]$ sudo systemctl status netdata
[sudo] password for lucas:
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 15:37:05 CEST; 5min ago
[...]
```
**Le service est paramétré pour démarrer au boot (enabled;) et est actif**
```
[lucas@web ~]$ sudo ss -alntp
State          Recv-Q         Send-Q                 Local Address:Port                    Peer Address:Port         Process
LISTEN         0              128                          0.0.0.0:22                           0.0.0.0:*             users:(("sshd",pid=825,fd=5))
LISTEN         0              128                        127.0.0.1:8125                         0.0.0.0:*             users:(("netdata",pid=2346,fd=45))
LISTEN         0              128                          0.0.0.0:19999                        0.0.0.0:*             users:(("netdata",pid=2346,fd=5))
LISTEN         0              128                                *:80                                 *:*             users:(("httpd",pid=1340,fd=4),("httpd",pid=862,fd=4),("httpd",pid=861,fd=4),("httpd",pid=858,fd=4),("httpd",pid=835,fd=4))
LISTEN         0              128                             [::]:22                              [::]:*             users:(("sshd",pid=825,fd=7))
LISTEN         0              128                            [::1]:8125                            [::]:*             users:(("netdata",pid=2346,fd=34))
LISTEN         0              128                             [::]:19999                           [::]:*             users:(("netdata",pid=2346,fd=6))
```
**Le service utilise le port 19999**
```
[lucas@web ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[lucas@web ~]$ sudo firewall-cmd --reload
success
```
**ajouter sur discord**
```
[lucas@web ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
[...]
###############################################################################
# discord (discordapp.com) global notification options
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897122080024301628/W8YZ5V9hKmJPLbazFaRTg2G686bkkjPprDqatbT-JyZlve8bJPiHlQgvucnF6C3whnJR"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"


[...]
```
**Vérifier le bon fonctionnement**
```
[lucas@web ~]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
[...]
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-12 11:58:38: alarm-notify.sh: INFO: sent discord notification for: web.tp2.linux test.chart.test_alarm is CLEAR to 'alarms'
# OK
```
**j'ai bien réçu les notifications sur discord**
```
[lucas@web ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```
**Config alerting**

**Sur `db.tp2.linux`**
```
[lucas@db ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 15:39:35 CEST; 3min 1s ago
[...]
```
**Le service est paramétré pour démarrer au boot (enabled;) et est actif**
```
[lucas@db ~]$ sudo ss -alntp
[sudo] password for lucas:
State     Recv-Q    Send-Q       Local Address:Port        Peer Address:Port   Process
LISTEN    0         128                0.0.0.0:22               0.0.0.0:*       users:(("sshd",pid=832,fd=5))
LISTEN    0         128              127.0.0.1:8125             0.0.0.0:*       users:(("netdata",pid=1712,fd=45))
LISTEN    0         128                0.0.0.0:19999            0.0.0.0:*       users:(("netdata",pid=1712,fd=5))
LISTEN    0         80                       *:3306                   *:*       users:(("mysqld",pid=925,fd=23))
LISTEN    0         128                   [::]:22                  [::]:*       users:(("sshd",pid=832,fd=7))
LISTEN    0         128                  [::1]:8125                [::]:*       users:(("netdata",pid=1712,fd=44))
LISTEN    0         128                   [::]:19999               [::]:*       users:(("netdata",pid=1712,fd=6))
```
**Le service utilise le port 19999**
```
[lucas@db ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[lucas@db ~]$ sudo firewall-cmd --reload
success
```
**ajouter sur discord**
```
[lucas@db ~]$ sudo /opt/netdata/etc/netdata/edit-config health_alarm_notify.conf
[...]
# discord (discordapp.com) global notification options
# sending discord notifications

# note: multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897122080024301628/W8YZ5V9hKmJPLbazFaRTg2G686bkkjPprDqatbT-JyZlve8bJPiHlQgvucnF6C3whnJR"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="alarms"
[...]
```
**Vérifier le bon functionnement**
```
[lucas@db ~]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test
[...]
--- BEGIN received response ---
ok
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-12 12:04:52: alarm-notify.sh: INFO: sent discord notification for: db.tp2.linux test.chart.test_alarm is CLEAR to 'alarms'
# OK
```
**Les notifications ont bien été reçues sur discord**
```
[lucas@db ~]$ sudo sed -i 's/curl=""/curl="\/opt\/netdata\/bin\/curl -k"/' /opt/netdata/etc/netdata/health_alarm_notify.conf
```
**Config alerting**

## II. Backup
### 2. Partage NFS
**Setup environnement**
```
[lucas@backup ~]$ sudo mkdir /srv/backup
[lucas@backup ~]$ sudo mkdir /srv/backup/web.tp2.linux
```
**Setup partage NFS**
```
[lucas@backup ~]$ sudo dnf install nfs-utils -y
[...]
[lucas@backup ~]$ sudo vim /etc/idmapd.conf
[...]
[lucas@backup ~]$ cat /etc/idmapd.conf
Domain = tp2.linux
[...]
[lucas@backup ~]$ sudo vim /etc/exports
[lucas@backup ~]$ cat /etc/exports
/srv/backup/web.tp2.linux 10.102.1.11(rw,no_root_squash)
[lucas@backup ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[lucas@backup ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
[lucas@backup ~]$ sudo firewall-cmd --reload
success
```
**Setup point de montage sur `web.tp2.linux`**
```
[lucas@web ~]$ sudo mkdir /srv/backup
[lucas@web ~]$ sudo dnf install nfs-utils -y
[...]
[lucas@web ~]$ sudo vim /etc/idmapd.conf
[lucas@web ~]$ cat /etc/idmapd.conf
[...]
Domain = tp2.linux
[...]
[lucas@web /]$ sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup
[lucas@web /]$ mount
[...]
backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime,vers=4.2,rsize=65536,wsize=65536,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
[lucas@web /]$ df -h
Filesystem                                  Size  Used Avail Use% Mounted on
devtmpfs                                    215M     0  215M   0% /dev
[...]
backup.tp2.linux:/srv/backup/web.tp2.linux  6.2G  2.2G  4.1G  35% /srv/backup
[lucas@web srv]$ touch /srv/backup/test
[lucas@web srv]$ ls /srv/backup/
test
```
**faire en sorte que la partition se monte automatiquement en démarrage**
```
[lucas@web srv]$ sudo vim /etc/fstab
[lucas@web srv]$ cat /etc/fstab
[...]
backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup  nfs     defaults        0 0
```
### 3. Backup de fichiers
**Rédiger le script de backup `/srv/tp2_backup.sh`**

[**tp2_backup.sh**](files/tp2_backup.sh)

**tester le bon fonctionnement**
```
[lucas@backup tests]$ mkdir test
[lucas@backup tests]$ cd test/
[lucas@backup test]$ vim test1
[lucas@backup test]$ cat test1
c'est le test oui
[lucas@backup test]$ touch test2
[lucas@backup test]$ touch test3
[lucas@backup tests]$ cd
[lucas@backup ~]$ mkdir archive
[lucas@backup ~]$ cd tests
[lucas@backup tests]$ ./tp2_backup.sh
[ERROR] you must specify a destination and a target.

usage: ./tp2_backup.sh destination target
        destination     Path to the directory which stores backups
        target          Path to the directory you want to backup
[lucas@backup tests]$ ./tp2_backup.sh /home/lucas/archive test
[ERROR] This script must be run as root.
[lucas@backup tests]$ sudo ./tp2_backup.sh /home/lucas/archive test
[OK] archive /home/lucas/tests/tp2_backup_20211025-210921.tar.gz created.
[OK] Archive /home/lucas/tests/tp2_backup_20211025-210921.tar.gz synchronized to /home/lucas/archive.
```
le test a l'air d'avoir fonctionné, on va vérifier le contenu de l'archive
```
[lucas@backup tests]$ cd /home/lucas/archive/
[lucas@backup archive]$ ls
tp2_backup_20211025-210921.tar.gz
[lucas@backup archive]$ tar -xf tp2_backup_20211025-210921.tar.gz
[lucas@backup archive]$ ls
test  tp2_backup_20211025-210921.tar.gz
[lucas@backup archive]$ cd test/
[lucas@backup test]$ ls
test1  test2  test3
[lucas@backup test]$ cat test1
c'est le test oui
```
Les fichiers sont bien intacts, le test a fonctionné
### 4.Unité de service
#### A.Unité de service
**créer une unité de service**
```
[lucas@backup test]$ sudo vim /etc/systemd/system/tp2_backup.service
[sudo] password for lucas:
[lucas@backup test]$ cat /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our smol backup service (Tp2)

[Service]
ExecStart=/home/lucas/tests/tp2_backup.sh /home/lucas/archive /home/lucas/tests/test
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```
**tester le fonctionnement**
```
[lucas@backup archive]$ ls
test  tp2_backup_20211025-210921.tar.gz
[lucas@backup archive]$ sudo systemctl daemon-reload
[sudo] password for lucas:
[lucas@backup archive]$ sudo systemctl start tp2_backup
[lucas@backup archive]$ ls
test  tp2_backup_20211025-210921.tar.gz  tp2_backup_20211025-213646.tar.gz
```
l'unité de service fonctionne
#### B.Timer
**créer le timer associé à `tp2_backup.service`**
```
[lucas@backup archive]$ sudo vim /etc/systemd/system/tp2_backup.timer
[sudo] password for lucas:
[lucas@backup archive]$ cat /etc/systemd/system/tp2_backup.timer
[Unit]
Description=Periodically run our smol TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```
**Activer le timer**
```
[lucas@backup archive]$ sudo systemctl daemon-reload
[sudo] password for lucas:
[lucas@backup archive]$ sudo systemctl start tp2_backup.timer
[lucas@backup archive]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
[lucas@backup archive]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our smol TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Mon 2021-10-25 21:58:27 CEST; 56s ago
  Trigger: Mon 2021-10-25 22:00:00 CEST; 35s left

Oct 25 21:58:27 backup.tp2.linux systemd[1]: Started Periodically run our smol TP2 backup script.
```
**vérifier que la backup s'exécute correctement**
```
[lucas@backup archive]$ ls
test                               tp2_backup_20211025-215827.tar.gz  tp2_backup_20211025-220101.tar.gz
tp2_backup_20211025-210921.tar.gz  tp2_backup_20211025-215915.tar.gz
tp2_backup_20211025-213646.tar.gz  tp2_backup_20211025-220015.tar.gz
```
le script s'exécute bien cependant il faudrait que j'ajoute le clear des anciens backups car ça va faire beaucoup
#### C.Contexte
**la backup s'exécute sur `web.tp2.linux`**
```
[lucas@web backup]$ sudo vim /etc/systemd/system/tp2_backup.timer
[lucas@web backup]$ sudo vim /etc/systemd/system/tp2_backup.service
[lucas@web backup]$ sudo systemctl daemon-reload
[lucas@web backup]$ sudo systemctl start tp2_backup.timer
[lucas@web backup]$ sudo systemctl enable tp2_backup.timer
Created symlink /etc/systemd/system/timers.target.wants/tp2_backup.timer → /etc/systemd/system/tp2_backup.timer.
[lucas@web backup]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer - Periodically run our smol TP2 backup script
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; enabled; vendor preset: disabled)
   Active: active (waiting) since Mon 2021-10-25 23:36:13 CEST; 16s ago
  Trigger: Tue 2021-10-26 03:15:00 CEST; 3h 38min left

Oct 25 23:36:13 web.tp2.linux systemd[1]: Started Periodically run our smol TP2 backup script.
[lucas@web backup]$ sudo systemctl list-timers
NEXT                          LEFT          LAST                          PASSED      UNIT                         A>
[...]
Tue 2021-10-26 03:15:00 CEST  3h 37min left n/a                           n/a         tp2_backup.timer             t>
[...]
```
le service va bien s'exécuter une fois qu'il sera 03h15

[**`tp2_backup.service`**](files/tp2_backup.service)

[**`tp2_backup.timer`**](files/tp2_backup.timer)