# TP1 : (re)Familiaration avec un système GNU/Linux
## 0.Préparation de la machine.
### Accès internet via la carte NAT:
```
[lucas@node1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:51:23:2a brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 78906sec preferred_lft 78906sec
    inet6 fe80::a00:27ff:fe51:232a/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
```
[lucas@node1 ~]$ ip r s
default via 10.0.2.2 dev enp0s3 proto dhcp metric 100
```
```
[lucas@node1 ~]$ ping 8.8.8.8 -c 4
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=23.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=31.3 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=26.2 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=22.7 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 22.681/25.918/31.343/3.398 ms
```
### Accès à un réseau local
```
[lucas@node1 ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:35:0f:d8 brd ff:ff:ff:ff:ff:ff
    inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe35:fd8/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
```
[lucas@node1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.11
NETMASK=255.255.255.0
```
```
[lucas@node2 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.101.1.12
NETMASK=255.255.255.0
```
```
[lucas@node1 ~]$ ping 10.101.1.12 -c 4
PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.773 ms
64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.581 ms
64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.871 ms
64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=0.930 ms

--- 10.101.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3083ms
rtt min/avg/max/mdev = 0.581/0.788/0.930/0.136 ms
```
### N'utiliser que ssh pour administrer les machines 
pour utiliser ssh:
```
PS C:\Users\hurlu> ssh lucas@10.101.1.11
```
### Les machines doivent avoir un nom
```
[lucas@node1 ~]$ sudo hostname node1.tp1.b2
```
```
[lucas@node1 ~]$ sudo cat /etc/hostname
node1.tp1.b2
```
```
[lucas@node1 ~]$ hostname
node1.tp1.b2
```
Pareil pour la deuxième machine
```
[lucas@node2 ~]$ hostname
node2.tp1.b2
```
### Utiliser le serveur DNS 1.1.1.1
```
[lucas@node1 ~]$ cat /etc/resolv.conf
nameserver 1.1.1.1
```
```
[lucas@node2 ~]$ cat /etc/resolv.conf
nameserver 1.1.1.1
```
```
[lucas@node1 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 38060
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10508   IN      A       92.243.16.143 <------ ip du nom demandé

;; Query time: 24 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)  <------ ip du serveur dns
;; WHEN: Wed Sep 22 16:59:24 CEST 2021
;; MSG SIZE  rcvd: 53
```
```
[lucas@node2 ~]$ dig ynov.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 5188
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143  <------ ip du nom demandé

;; Query time: 36 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)   <------ ip du serveur dns
;; WHEN: Sun Sep 26 22:17:31 CEST 2021
;; MSG SIZE  rcvd: 53
```
### Les machines doivent pouvoir se joindre par leurs noms respectifs
```
[lucas@node1 ~]$ cat /etc/hosts
10.101.1.12 node2.tp1.b2 node2
```
```
[lucas@node2 ~]$ cat /etc/hosts
10.101.1.11 node1.tp1.b2 node1
```
```
[lucas@node1 ~]$ ping -c 4 node2.tp1.b2
PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.459 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.880 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=3 ttl=64 time=0.908 ms
64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=4 ttl=64 time=0.862 ms

--- node2.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3023ms
rtt min/avg/max/mdev = 0.459/0.777/0.908/0.185 ms
```
```
[lucas@node2 ~]$ ping -c4 node1.tp1.b2
PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.426 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.565 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=0.470 ms
64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=4 ttl=64 time=0.550 ms

--- node1.tp1.b2 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3108ms
rtt min/avg/max/mdev = 0.426/0.502/0.565/0.063 ms
```
### le pare feu doit être configuré pour bloquer toutes les connexions non nécessaires
```
[lucas@node1 ~]$ sudo firewall-cmd --list-all
[sudo] password for lucas:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
```
[lucas@node2 ~]$ sudo firewall-cmd --list-all
[sudo] password for lucas:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
## I. Utilisateurs
###  1. Création et configuration
```
[lucas@node1 ~]$ sudo useradd admin -m -s /bin/bash
```
```
[lucas@node2 ~]$ sudo useradd admin -m -s /bin/bash
```
```
[lucas@node1 ~]$ ls /home
admin  lucas
```
```
[lucas@node1 ~]$ sudo groupadd admins
```
```
[lucas@node2 ~]$ sudo groupadd admins
```
```
## Allows people in group wheel to run all commands
%admins ALL=(ALL)       ALL
```
Comme le groupe "wheel" n'a plus de permissions je suis passé par root pour ajouter admin au groupe admins
```
[root@node1 lucas]# usermod -aG admins admin
```
```
[root@node2 lucas]# usermod -aG admins admin
```
### 2.SSH
```
PS C:\Users\hurlu> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\hurlu/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
[...]
PS C:\Users\hurlu> cat .\.ssh\id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDA62+Bpgut82XXz2ufFogqw1wh1RDdFmX2FFC10cQ6htpWuWSQ7Q1QhxsM8/Ble9HazJv9Fp+0uCV23nxw/VdQNL3b/AAOAi8RUgkccId4ljcPVol22RQuTxVW2HGk/PIVdu3ZdLtjU6CNNt+3uEAz3Y/wWKfkwEcxvN4hGQ/uNYAcZulkAE9BONJabNQXLBBfGRZWRXFYfJG3yDD440OZCBvCYQ3gyhChpl9fO7D2t2ekzT4gSA0PsK+aNkGygV+WtD0TDztadnPhMyMaKMAS0S4cFa59N9jO5I5Bih0lZ3q1t5FwW14aFILxqfFLbl7wtXdJ5pJSMUvw5DtB3widXeZzDWfW5M1EFAOOhtNj/cWqLzKD/56o1me+qa0qMxeFn4SZ//osJzsmJKVNtQK0ua8iG/Oyp6Aws5Ohg4u8MB3aayX0dgo2CLs7hP6kCeMSmZHag18SjmryIGHdi5m0Sq02Y3D5hRyZceNrkBrNoj95G6Dfqwf/p8AskMo2N5A1okJl9PT00f7PE4yaG7SGTHnpbNQdiWgiGerIHSCpP0oBiYBJ5eiBxbvbK0CwgNjpDRDV9hrMG2DXgJYmwl2NOYSGj1sdm2JPZO1t7gmhsLh2POgvz36Rdr4gquuw9fi5wd2qLwqtUM82OasknG/CxF504Vv4lnG4x2YArF8j9w== hurlu@LAPTOP-4DFELSLI
```
Ajout des clés sur les machines:
```
[admin@node1 ~]$ mkdir .ssh
[admin@node1 ~]$ cd .ssh
[admin@node1 .ssh]$ nano authorized_keys
[admin@node1 .ssh]$ cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDA62+Bpgut82XXz2ufFogqw1wh1RDdFmX2FFC10cQ6htpWuWSQ7Q1QhxsM8/Ble9HazJv9Fp+0uCV23nxw/VdQNL3b/AAOAi8RUgkccId4ljcPVol22RQuTxVW2HGk/PIVdu3ZdLtjU6CNNt+3uEAz3Y/wWKfkwEcxvN4hGQ/uNYAcZulkAE9BONJabNQXLBBfGRZWRXFYfJG3yDD440OZCBvCYQ3gyhChpl9fO7D2t2ekzT4gSA0PsK+aNkGygV+WtD0TDztadnPhMyMaKMAS0S4cFa59N9jO5I5Bih0lZ3q1t5FwW14aFILxqfFLbl7wtXdJ5pJSMUvw5DtB3widXeZzDWfW5M1EFAOOhtNj/cWqLzKD/56o1me+qa0qMxeFn4SZ//osJzsmJKVNtQK0ua8iG/Oyp6Aws5Ohg4u8MB3aayX0dgo2CLs7hP6kCeMSmZHag18SjmryIGHdi5m0Sq02Y3D5hRyZceNrkBrNoj95G6Dfqwf/p8AskMo2N5A1okJl9PT00f7PE4yaG7SGTHnpbNQdiWgiGerIHSCpP0oBiYBJ5eiBxbvbK0CwgNjpDRDV9hrMG2DXgJYmwl2NOYSGj1sdm2JPZO1t7gmhsLh2POgvz36Rdr4gquuw9fi5wd2qLwqtUM82OasknG/CxF504Vv4lnG4x2YArF8j9w== hurlu@LAPTOP-4DFELSLI
[admin@node2 ~]$ sudo chmod 600 /home/admin/.ssh/authorized_keys
[admin@node2 ~]$ sudo chmod 700 /home/admin/.ssh
```
```
[admin@node1 ~]$ mkdir .ssh
[admin@node1 ~]$ cd .ssh
[admin@node1 .ssh]$ nano authorized_keys
[admin@node1 .ssh]$ cat authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDA62+Bpgut82XXz2ufFogqw1wh1RDdFmX2FFC10cQ6htpWuWSQ7Q1QhxsM8/Ble9HazJv9Fp+0uCV23nxw/VdQNL3b/AAOAi8RUgkccId4ljcPVol22RQuTxVW2HGk/PIVdu3ZdLtjU6CNNt+3uEAz3Y/wWKfkwEcxvN4hGQ/uNYAcZulkAE9BONJabNQXLBBfGRZWRXFYfJG3yDD440OZCBvCYQ3gyhChpl9fO7D2t2ekzT4gSA0PsK+aNkGygV+WtD0TDztadnPhMyMaKMAS0S4cFa59N9jO5I5Bih0lZ3q1t5FwW14aFILxqfFLbl7wtXdJ5pJSMUvw5DtB3widXeZzDWfW5M1EFAOOhtNj/cWqLzKD/56o1me+qa0qMxeFn4SZ//osJzsmJKVNtQK0ua8iG/Oyp6Aws5Ohg4u8MB3aayX0dgo2CLs7hP6kCeMSmZHag18SjmryIGHdi5m0Sq02Y3D5hRyZceNrkBrNoj95G6Dfqwf/p8AskMo2N5A1okJl9PT00f7PE4yaG7SGTHnpbNQdiWgiGerIHSCpP0oBiYBJ5eiBxbvbK0CwgNjpDRDV9hrMG2DXgJYmwl2NOYSGj1sdm2JPZO1t7gmhsLh2POgvz36Rdr4gquuw9fi5wd2qLwqtUM82OasknG/CxF504Vv4lnG4x2YArF8j9w== hurlu@LAPTOP-4DFELSLI
[admin@node1 ~]$ sudo chmod 600 /home/admin/.ssh/authorized_keys
[sudo] password for admin:
[admin@node1 ~]$ sudo chmod 700 /home/admin/.ssh
```
#### La connexion SSH est elle fonctionnelle sans mot de passe ?
```
PS C:\Users\hurlu\.ssh> ssh admin@10.101.1.11
admin@10.101.1.11's password:
```
```
PS C:\Users\hurlu\.ssh> ssh admin@10.101.1.12
admin@10.101.1.12's password:
```
Malheureusement non, j'ai toujours besoin d'un mot de passe pour une raison obscure.

## II. Partitionnement
Repérer les disques
```
[lucas@node1 ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    3G  0 disk
sdc           8:32   0    3G  0 disk
sr0          11:0    1 1024M  0 rom
```

```
[lucas@node1 ~]$ sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[lucas@node1 ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.

[lucas@node1 ~]$ sudo vgcreate volume /dev/sdb
  Volume group "volume" successfully created
[lucas@node1 ~]$ sudo vgextend volume /dev/sdc
  Volume group "volume" successfully extended

[lucas@node1 ~]$ sudo lvcreate -L 1G volume -n part1
  Logical volume "part1" created.
[lucas@node1 ~]$ sudo lvcreate -L 1G volume -n part2
  Logical volume "part2" created.
[lucas@node1 ~]$ sudo lvcreate -L 1G volume -n part3
  Logical volume "part3" created.

[lucas@node1 ~]$ sudo lvs
  LV    VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root  rl     -wi-ao----  <6.20g
  swap  rl     -wi-ao---- 820.00m
  part1 volume -wi-a-----   1.00g
  part2 volume -wi-a-----   1.00g
  part3 volume -wi-a-----   1.00g

[lucas@node1 ~]$ sudo mkfs -t ext4 /dev/volume/part1
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: f2d03d05-967c-412d-ae59-8041ae763807
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[lucas@node1 ~]$ sudo mkfs -t ext4 /dev/volume/part2
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: 1c137dcd-d858-440d-a05a-34f5cc21a1df
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[lucas@node1 ~]$ sudo mkfs -t ext4 /dev/volume/part3
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 262144 4k blocks and 65536 inodes
Filesystem UUID: e26297e2-5d9f-4f7a-b0a4-4ad9a2376c83
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done

[lucas@node1 mnt]$ sudo mkdir part1
[lucas@node1 mnt]$ sudo mkdir part2
[lucas@node1 mnt]$ sudo mkdir part3
[lucas@node1 mnt]$ sudo mount /dev/volume/part1 /mnt/part1
[lucas@node1 mnt]$ sudo mount /dev/volume/part2 /mnt/part2
[lucas@node1 mnt]$ sudo mount /dev/volume/part3 /mnt/part3

[lucas@node1 mnt]$ mount
[...]
/dev/mapper/volume-part1 on /mnt/part1 type ext4 (rw,relatime,seclabel)
/dev/mapper/volume-part2 on /mnt/part2 type ext4 (rw,relatime,seclabel)
/dev/mapper/volume-part3 on /mnt/part3 type ext4 (rw,relatime,seclabel)

[lucas@node1 ~]$ sudo cat /etc/fstab
[...]
/dev/volume/part1 /mnt/part1 ext4 defaults 0 0
/dev/volume/part2 /mnt/part2 ext4 defaults 0 0
/dev/volume/part3 /mnt/part3 ext4 defaults 0 0

[lucas@node1 mnt]$ sudo umount /mnt/part1
[lucas@node1 mnt]$ sudo umount /mnt/part2
[lucas@node1 mnt]$ sudo umount /mnt/part3
[lucas@node1 mnt]$ sudo mount -av
[...]
/mnt/part1               : successfully mounted
[...]
/mnt/part2               : successfully mounted
[...]
/mnt/part3               : successfully mounted
```
## III.Gestion des services
### 1. Interaction avec un service existant
```
[lucas@node1 ~]$ systemctl is-active firewalld
active
[lucas@node1 ~]$ systemctl is-enabled firewalld
enabled
```
firewalld est actif et se lance au démarrage
### 2. Création de service
#### A.Unité simpliste
```
[lucas@node1 ~]$ sudo cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```
ouverture du port 8888
```
[lucas@node1 ~]$ sudo firewall-cmd --zone=drop --add-port=8888/tcp
[sudo] password for lucas:
success
[lucas@node1 ~]$ sudo firewall-cmd --zone=drop --add-service=http --permanent
success
```
interactions avec le service
```
[lucas@node1 ~]$ sudo systemctl daemon-reload
[lucas@node1 ~]$ sudo systemctl start web
[lucas@node1 ~]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
[lucas@node1 ~]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-09-27 02:06:57 CEST; 11s ago
 Main PID: 4626 (python3)
    Tasks: 1 (limit: 11385)
   Memory: 9.6M
   CGroup: /system.slice/web.service
           └─4626 /bin/python3 -m http.server 8888

Sep 27 02:06:57 node1.tp1.b2 systemd[1]: Started Very simple web service.
```
verification du fonctionnement avec curl
```
[lucas@node1 ~]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
```
ajout de l'utilisateur web
```
[lucas@node1 ~]$ sudo useradd web
[lucas@node1 ~]$ sudo usermod -aG admins web
```
Vérification de la configuration du serveur
```
[lucas@node1 srv]$ sudo cat /etc/systemd/system/web.service
[sudo] password for lucas:
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/srv1

[Install]
WantedBy=multi-user.target
```
Création du fichier dans le dossier du serveur
```
[web@node1 srv]$ sudo mkdir srv1
[web@node1 srv]$ cd srv1/
[web@node1 srv1]$ sudo nano test.txt
```
Vérification du bon fonctionnement avec curl
```
[lucas@node1 srv]$ sudo systemctl stop web
[lucas@node1 srv]$ sudo systemctl daemon-reload
[lucas@node1 srv]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="test.txt">test.txt</a></li>
</ul>
<hr>
</body>
</html>
```